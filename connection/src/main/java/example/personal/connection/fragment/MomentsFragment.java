package example.personal.connection.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import example.personal.connection.MyApplication;
import example.personal.connection.R;
import example.personal.connection.activity.MomentsListActivity;
import example.personal.connection.activity.MyPostListActivity;
import example.personal.connection.activity.SendMonmentsActivity;

/**
 * Created by wuqian on 2017/2/22.
 * mail: wuqian@ilingtong.com
 * Description:
 */
public class MomentsFragment extends Fragment implements View.OnClickListener{

    private ImageView titleBarImgBack;
    private TextView titleBarTxtTitle;
    private TextView momentsTxtMonments;
    private TextView monmentsTxtMyPost;
    private TextView monmentsTxtPublish;


    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_moments_layout, null);
        iniView(view);
        return view;
    }

    private void iniView(View view) {
        titleBarImgBack = (ImageView) view.findViewById(R.id.title_bar_img_back);
        titleBarImgBack.setVisibility(View.GONE);
        titleBarTxtTitle = (TextView) view.findViewById(R.id.title_bar_txt_title);
        titleBarTxtTitle.setText(getString(R.string.app_name));
        momentsTxtMonments = (TextView) view.findViewById(R.id.moments_txt_monments);
        monmentsTxtMyPost = (TextView) view.findViewById(R.id.monments_txt_my_post);
        monmentsTxtPublish = (TextView) view.findViewById(R.id.monments_txt_publish);

        momentsTxtMonments.setOnClickListener(this);
        monmentsTxtMyPost.setOnClickListener(this);
        monmentsTxtPublish.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {

        switch (view.getId()){
            case R.id.moments_txt_monments:  //朋友圈
                MomentsListActivity.launch(getActivity());
                break;
            case R.id.monments_txt_my_post:  //自己的朋友圈
                MyPostListActivity.launch(getActivity(), MyApplication.getInstance().getUserInfo());
                break;
            case R.id.monments_txt_publish:   //发朋友圈
                Intent intent = new Intent(getActivity(), SendMonmentsActivity.class);
                getActivity().startActivity(intent);
                break;
        }
    }
}
