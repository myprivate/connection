package example.personal.connection.fragment;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.nostra13.universalimageloader.core.ImageLoader;

import example.personal.connection.Constants;
import example.personal.connection.MyApplication;
import example.personal.connection.R;
import example.personal.connection.Utils.CommonUtils;
import example.personal.connection.activity.BoardListActicity;
import example.personal.connection.activity.LoginActvity;
import example.personal.connection.activity.MainFragmentActivity;
import example.personal.connection.activity.MyInfomationActivity;
import example.personal.connection.module.emchat.EMChatManager;

/**
 * Created by wuqian on 2017/2/22.
 * mail: wuqian@ilingtong.com
 * Description:我的
 */
public class MeFragment extends Fragment implements View.OnClickListener {

    private ImageView titleBarImgBack;
    private TextView titleBarTxtTitle;
    private ImageView meImgHead;
    private TextView momentsTxtMyInfo;
    private TextView monmentsTxtBorad;
    private TextView txtName;
    private TextView txtId;
    private Button btnexit;
    private SharedPreferences sp;


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_me_layout, null);
        initView(view);
        sp = getActivity().getSharedPreferences("login", Context.MODE_PRIVATE);
        setData();
        return view;
    }

    private void initView(View view) {
        titleBarImgBack = (ImageView) view.findViewById(R.id.title_bar_img_back);
        titleBarImgBack.setVisibility(View.GONE);
        titleBarTxtTitle = (TextView) view.findViewById(R.id.title_bar_txt_title);
        titleBarTxtTitle.setText(getString(R.string.app_name));
        meImgHead = (ImageView) view.findViewById(R.id.me_img_head);
        momentsTxtMyInfo = (TextView) view.findViewById(R.id.me_txt_myinfo);
        monmentsTxtBorad = (TextView) view.findViewById(R.id.me_txt_boradlist);
        txtName = (TextView) view.findViewById(R.id.me_txt_name);
        txtId = (TextView) view.findViewById(R.id.me_txt_id);
        btnexit = (Button) view.findViewById(R.id.btnexit);

        momentsTxtMyInfo.setOnClickListener(this);
        monmentsTxtBorad.setOnClickListener(this);
        btnexit.setOnClickListener(this);
    }

    private void setData() {
        if (!TextUtils.isEmpty(MyApplication.getInstance().getUserInfo().photo)) {
            ImageLoader.getInstance().displayImage(Constants.formatImageUrl(MyApplication.getInstance().getUserInfo().photo), meImgHead, CommonUtils.getUserIconOptions());
        }
        txtId.setText(String.format(getResources().getString(R.string.me_id), MyApplication.getInstance().getUserInfo().phone));
        txtName.setText(MyApplication.getInstance().getUserInfo().name);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.me_txt_myinfo:  //去我的信息
                Intent intentInfo = new Intent(getActivity(), MyInfomationActivity.class);
                startActivityForResult(intentInfo,1000);
                break;
            case R.id.me_txt_boradlist:  //去公告列表
                Intent intentBorad = new Intent(getActivity(), BoardListActicity.class);
                startActivity(intentBorad);
                break;
            case R.id.btnexit:  //退出登录
                Intent intent = new Intent(getActivity(), LoginActvity.class);
                startActivity(intent);
                EMChatManager.getInstance().syncLoginOutEM();
                sp.edit().putString("password","").commit();
                MainFragmentActivity.instance.finish();
                break;
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == 1000){
            setData();
        }
    }
}
