package example.personal.connection.fragment;

import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.android.volley.Response;
import com.android.volley.VolleyError;

import java.util.ArrayList;
import java.util.List;

import example.personal.connection.Constants;
import example.personal.connection.MyApplication;
import example.personal.connection.R;
import example.personal.connection.Utils.DialogUtils;
import example.personal.connection.Utils.ToastUtils;
import example.personal.connection.activity.FriendApplyListActivity;
import example.personal.connection.activity.ProfileActivity;
import example.personal.connection.activity.SearchActivity;
import example.personal.connection.adapter.ContactAdapter;
import example.personal.connection.entity.ContactResult;
import example.personal.connection.entity.LoginInfoBean;
import example.personal.connection.entity.LoginResultBean;
import example.personal.connection.manager.ServiceManager;

/**
 * Created by wuqian on 2017/2/22.
 * mail: wuqian@ilingtong.com
 * Description:我的校友
 */
public class AlumniFragment extends Fragment implements View.OnClickListener {

    private ImageView titleBarImgBack;
    private TextView titleBarTxtTitle;
    // private SearchView alumniSvSearch;
    private EditText editSearch;
    private LinearLayout llNewFriend;
    private ListView messageList;
    private ContactAdapter adapter;
    private Dialog dialog;
    private List<LoginInfoBean> list = new ArrayList<>();


    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_alumni_layout, null);
        initView(view);

        adapter = new ContactAdapter(getContext(), list);
        messageList.setAdapter(adapter);
        return view;
    }

    @Override
    public void onHiddenChanged(boolean hidden) {
        Log.e("TAG", "onHiddenChanged hidden=" + hidden);
        super.onHiddenChanged(hidden);
        if (!hidden) {
            getContactList();
        }
    }

    /**
     * 初始化view
     *
     * @param view
     */
    private void initView(View view) {
        titleBarImgBack = (ImageView) view.findViewById(R.id.title_bar_img_back);
        titleBarImgBack.setVisibility(View.GONE);
        titleBarTxtTitle = (TextView) view.findViewById(R.id.title_bar_txt_title);
        titleBarTxtTitle.setText(getString(R.string.app_name));

        View headView = LayoutInflater.from(getActivity()).inflate(R.layout.alumni_list_headview_layout, null);
        editSearch = (EditText) headView.findViewById(R.id.alumni_sv_search);
        llNewFriend = (LinearLayout) headView.findViewById(R.id.alumni_ll_newfriend);
        llNewFriend.setOnClickListener(this);

        messageList = (ListView) view.findViewById(R.id.message_list);
        messageList.addHeaderView(headView);
        editSearch.setOnClickListener(this);
        messageList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                getUserInfo(list.get(i - 1).id);
            }
        });
        dialog = DialogUtils.createLoadingDialog(getContext());
    }

    /**
     * 获取好友简介
     *
     * @param userid
     */
    private void getUserInfo(String userid) {
        dialog.show();
        ServiceManager.getUserInfo(userid, successListener(), new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                dialog.dismiss();
                ToastUtils.showLongToast(getActivity(), getString(R.string.syserro));
            }
        });
    }

    /**
     * 获取个人信息成功回调
     *
     * @return
     */
    private Response.Listener<LoginResultBean> successListener() {
        return new Response.Listener<LoginResultBean>() {
            @Override
            public void onResponse(LoginResultBean response) {
                dialog.dismiss();
                if (Constants.SUCCESSCODE.equals(response.code)) {

                    ProfileActivity.launch(AlumniFragment.this, response.data, 1001);
                } else {
                    ToastUtils.showLongToast(getActivity(), response.message);
                }
            }
        };
    }

    /**
     * 获取我的校友列表
     */
    private void getContactList() {
        dialog.show();
        ServiceManager.getContactList(MyApplication.getInstance().getUserInfo().id, listener(), new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                dialog.dismiss();
            }
        });
    }

    private Response.Listener<ContactResult> listener() {
        return new Response.Listener<ContactResult>() {
            @Override
            public void onResponse(ContactResult response) {
                dialog.dismiss();
                if (Constants.SUCCESSCODE.equals(response.code)) {
                    Log.e("TAG", "response.data.size=" + response.data.size());
                    list = response.data;
                    adapter.setList(list);
                    llNewFriend.setVisibility(response.extraData > 0 ? View.VISIBLE : View.GONE);
                }
            }
        };
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {

        if (resultCode == Activity.RESULT_OK) {
            switch (requestCode) {
                case 1000:   //从好友申请列表返回到该页面，添加了好友，刷新好友列表
                    getContactList();
                    break;
                case 1001:  //从好友个人信息页面返回到该页面，删除了还有，刷新好友列表
                    getContactList();
                    break;
            }
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.alumni_sv_search:
                Intent intent = new Intent(getActivity(), SearchActivity.class);
                getActivity().startActivity(intent);
                break;
            case R.id.alumni_ll_newfriend:  //好友申请列表
                Intent intent1 = new Intent(getActivity(), FriendApplyListActivity.class);
                startActivityForResult(intent1, 1000);
                break;
        }
    }
}
