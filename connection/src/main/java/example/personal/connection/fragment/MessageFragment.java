package example.personal.connection.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.hyphenate.EMMessageListener;
import com.hyphenate.chat.EMClient;
import com.hyphenate.chat.EMConversation;
import com.hyphenate.chat.EMMessage;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Map;

import example.personal.connection.Constants;
import example.personal.connection.MyApplication;
import example.personal.connection.R;
import example.personal.connection.Utils.DateUtils;
import example.personal.connection.activity.IMChatActivity;
import example.personal.connection.adapter.MessageAdapter;
import example.personal.connection.entity.ConversationResultBean;
import example.personal.connection.entity.LoginInfoBean;
import example.personal.connection.manager.ServiceManager;
import example.personal.connection.module.emchat.EMChatManager;
import example.personal.connection.module.emchat.IMMessage;

/**
 * Created by wuqian on 2017/2/22.
 * mail: wuqian@ilingtong.com
 * Description:
 */
public class MessageFragment extends Fragment implements AdapterView.OnItemClickListener{

    private ImageView titleBarImgBack;
    private TextView titleBarTxtTitle;
    private ListView messageList;
    private MessageAdapter mAdapter = null;
    private List<IMMessage> mDataSet = null;
    private List<ConversationResultBean.ConversationBean> mInfoSet = null;
    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        mDataSet = new ArrayList<>();
        mAdapter = new MessageAdapter(getActivity(),mDataSet,R.layout.layout_message_item);
        EMChatManager.getInstance().addMessageListener(new EMMessageListener() {
            @Override
            public void onMessageReceived(List<EMMessage> messages) {
                Log.e("MessageFragment","onMessageReceived");
                getMessageList();
            }

            @Override
            public void onCmdMessageReceived(List<EMMessage> messages) {

            }

            @Override
            public void onMessageRead(List<EMMessage> messages) {

            }

            @Override
            public void onMessageDelivered(List<EMMessage> messages) {

            }

            @Override
            public void onMessageChanged(EMMessage message, Object change) {

            }
        });
        EMChatManager.getInstance().addConversationFlushListener(new EMChatManager.FlushConversationListener() {
            @Override
            public void flush() {
                getMessageList();
            }
        });
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        Log.e("TAG","onCreateView");
        View view = inflater.inflate(R.layout.fragment_message_layout, null);
        initView(view);
        return view;
    }

    @Override
    public void onHiddenChanged(boolean hidden) {
        if(!hidden){
            getMessageList();
        }
        super.onHiddenChanged(hidden);
    }

    /**
     * 获取联系人
     */
    private void getMessageList(){
        String id = MyApplication.getInstance().getUserId();
        ServiceManager.getConversationList(id, new Response.Listener<ConversationResultBean>() {
            @Override
            public void onResponse(ConversationResultBean response) {
                if(Constants.SUCCESSCODE.equals(response.code)){
                    mInfoSet = response.data;
                    handlerData(response.data);
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                if(isAdded()){
                    Toast.makeText(getActivity(), getString(R.string.syserro), Toast.LENGTH_LONG).show();
                }
            }
        });
    }

    private void handlerData(List<ConversationResultBean.ConversationBean> data){
        Log.e("TAG","handlerData");
        sortByModifyTime(data);
        mDataSet.clear();
        Map<String,EMConversation> conversations = EMClient.getInstance().chatManager().getAllConversations();
        List<IMMessage> messages = new ArrayList<>(data.size());
        IMMessage message = null;
        ConversationResultBean.ConversationBean bean = null;
        EMConversation conversation = null;
        for (int i = 0;i < data.size();i++){
            message = new IMMessage();
            bean = data.get(i);
            conversation = conversations.get(bean.friendVoipUserName);
            message.sendPhoto = bean.friendPhoto;
            message.sendName = bean.friendName;
            if(conversation != null){
                message.message = conversation.getLastMessage();
                message.unReadCount = conversation.getUnreadMsgCount();
            }
            mDataSet.add(message);
        }
        mAdapter.setData(mDataSet);
        messageList.setAdapter(mAdapter);
        mAdapter.notifyDataSetChanged();
    }

    private void sortByModifyTime(List<ConversationResultBean.ConversationBean> data){
        if(data == null){
            return;
        }
        Collections.sort(data, new Comparator<ConversationResultBean.ConversationBean>() {
            @Override
            public int compare(ConversationResultBean.ConversationBean o1, ConversationResultBean.ConversationBean o2) {
                Calendar c = Calendar.getInstance();
                c.setTimeInMillis(DateUtils.parseFormatGMTLongDate(o1.modifyTime));
                Calendar c2 = Calendar.getInstance();
                c2.setTimeInMillis(DateUtils.parseFormatGMTLongDate(o2.modifyTime));
                if(c.after(c2)){
                    return -1;
                }else if(c.before(c2)){
                    return 1;
                }
                return 0;
            }
        });
    }

    private void initView(View view) {
        titleBarImgBack = (ImageView) view.findViewById(R.id.title_bar_img_back);
        titleBarImgBack.setVisibility(View.GONE);
        titleBarTxtTitle = (TextView) view.findViewById(R.id.title_bar_txt_title);
        titleBarTxtTitle.setText(getString(R.string.app_name));
        messageList = (ListView) view.findViewById(R.id.message_list);
        messageList.setAdapter(mAdapter);
        messageList.setOnItemClickListener(this);
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        Intent intent = new Intent(getActivity(),IMChatActivity.class);
        IMMessage msg = new IMMessage();
        LoginInfoBean info = MyApplication.getInstance().getUserInfo();
        if(null != info) {
            msg.sendId = info.id;
            msg.sendName = info.name;
            msg.sendPhoto =info.photo;
            msg.sendUserName = info.voipUsername;
        }
        msg.receiveId = mInfoSet.get(position).id;
        msg.receiveName = mInfoSet.get(position).friendName;
        msg.receivePhoto = mInfoSet.get(position).friendPhoto;
        msg.receiveUserName = mInfoSet.get(position).friendVoipUserName;
        intent.putExtra("content",msg);
        EMChatManager.getInstance().clearNotification();
        startActivityForResult(intent,100);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        Log.e("TAG","onActivityResult");
        handlerData(mInfoSet);
        super.onActivityResult(requestCode, resultCode, data);
    }
}
