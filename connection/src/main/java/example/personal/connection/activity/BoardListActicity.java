package example.personal.connection.activity;

import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.volley.Response;
import com.android.volley.VolleyError;

import java.util.ArrayList;
import java.util.List;

import example.personal.connection.Constants;
import example.personal.connection.R;
import example.personal.connection.Utils.DialogUtils;
import example.personal.connection.Utils.ToastUtils;
import example.personal.connection.adapter.BoardListAdapter;
import example.personal.connection.entity.BoardListResult;
import example.personal.connection.manager.ServiceManager;
import example.personal.connection.widget.XListView;

/**
 * Created by wuqian on 2017/2/23.
 * mail: wuqian@ilingtong.com
 * Description:公告栏列表
 */
public class BoardListActicity extends Activity implements XListView.IXListViewListener {
    private ImageView titleBarImgBack;
    private TextView titleBarTxtTitle;
    private TextView titleBarTxtAction;
    private XListView boardListView;
    private BoardListAdapter adapter;
    private List<BoardListResult.BoardBean> list = new ArrayList<>();
    private Dialog dialog;
    private int currentPage = 1;
    private final int WHAT_REFRESH_LIST = 0;
    Handler handler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case WHAT_REFRESH_LIST:
                    boardListView.setRefreshTime();
                    adapter.notifyDataSetChanged();
                    break;
            }
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_boardlist_layout);
        initView();
        getData(0);
    }

    /**
     * 初始化界面
     */
    private void initView() {
        dialog = DialogUtils.createLoadingDialog(this);
        titleBarImgBack = (ImageView) findViewById(R.id.title_bar_img_back);
        titleBarImgBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                BoardListActicity.this.finish();
            }
        });
        titleBarTxtTitle = (TextView) findViewById(R.id.title_bar_txt_title);
        titleBarTxtTitle.setText(getString(R.string.bulletinBoard));
        titleBarTxtAction = (TextView) findViewById(R.id.title_bar_txt_action);
        titleBarTxtAction.setText("New Notice");
        titleBarTxtAction.setVisibility(View.VISIBLE);
        titleBarTxtAction.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(BoardListActicity.this, BoardEditActivity.class);
                startActivityForResult(intent, 1000);
            }
        });
        boardListView = (XListView) findViewById(R.id.board_list);
        boardListView.setPullLoadEnable(false);
        boardListView.setPullRefreshEnable(true);
        boardListView.setXListViewListener(this, 0);
        boardListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                BoardDetailActivity.launch(BoardListActicity.this, list.get(i - 1));
            }
        });
        adapter = new BoardListAdapter(list, this);
        boardListView.setAdapter(adapter);
    }

    private void getData(int pageNum) {
        ServiceManager.getBoradList(Constants.PAGE_SIZE, pageNum, listener(), new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                ToastUtils.showLongToast(BoardListActicity.this, error.getMessage() + "");
            }
        });
    }

    private Response.Listener<BoardListResult> listener() {
        return new Response.Listener<BoardListResult>() {
            @Override
            public void onResponse(BoardListResult response) {
                if (Constants.SUCCESSCODE.equals(response.code)) {
                    list.addAll(response.data.data);
                    handler.sendEmptyMessage(WHAT_REFRESH_LIST);
                    currentPage = response.data.pageNum;
                    boardListView.setPullLoadEnable(!response.data.lastPage);
                } else {
                    ToastUtils.showLongToast(BoardListActicity.this, response.message);
                }
            }
        };
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == 1000 && resultCode == RESULT_OK) {
            onRefresh(0);
        }
    }

    @Override
    public void onRefresh(int id) {
        currentPage = 1;
        list.clear();
        getData(currentPage);
    }

    @Override
    public void onLoadMore(int id) {
        getData(currentPage + 1);
    }
}
