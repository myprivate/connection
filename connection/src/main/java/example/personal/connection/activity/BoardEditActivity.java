package example.personal.connection.activity;

import android.app.Activity;
import android.app.Dialog;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.volley.Response;
import com.android.volley.VolleyError;

import example.personal.connection.Constants;
import example.personal.connection.R;
import example.personal.connection.Utils.DialogUtils;
import example.personal.connection.Utils.ToastUtils;
import example.personal.connection.entity.BaseBean;
import example.personal.connection.manager.ServiceManager;

/**
 * Created by wuqian on 2017/3/28.
 * mail: wuqian@ilingtong.com
 * Description:编辑公告
 */
public class BoardEditActivity extends Activity implements View.OnClickListener {
    private ImageView titleBarImgBack;
    private TextView titleBarTxtTitle;
    private TextView titleBarTxtAction;
    private EditText boardEditEditTitle;
    private EditText boardEditEditContent;
    private Dialog dialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_board_edit_layout);
        init();
    }
    /**
     * 初始化
     */
    private void init() {
        dialog = DialogUtils.createLoadingDialog(this);
        titleBarImgBack = (ImageView) findViewById(R.id.title_bar_img_back);
        titleBarImgBack.setOnClickListener(this);
        titleBarTxtTitle = (TextView) findViewById(R.id.title_bar_txt_title);
        titleBarTxtTitle.setText("Edit Notice");
        titleBarTxtAction = (TextView) findViewById(R.id.title_bar_txt_action);
        titleBarTxtAction.setText("Save");
        titleBarTxtAction.setVisibility(View.VISIBLE);
        titleBarTxtAction.setOnClickListener(this);
        boardEditEditTitle = (EditText) findViewById(R.id.board_edit_edit_title);
        boardEditEditContent = (EditText) findViewById(R.id.board_edit_edit_content);
    }

    /**
     * 发布公告
     */
    private void editNotice() {
        if (TextUtils.isEmpty(boardEditEditTitle.getText().toString().trim())) {
            ToastUtils.showShortToast(this, "Title must be filled");
            return;
        }
        if (TextUtils.isEmpty(boardEditEditContent.getText().toString().trim())) {
            ToastUtils.showShortToast(this, "Content must be filled");
            return;
        }
        dialog.show();
        ServiceManager.publishBorad(boardEditEditTitle.getText().toString().trim(), boardEditEditContent.getText().toString().trim(), listener(), new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                dialog.dismiss();
                ToastUtils.showLongToast(BoardEditActivity.this,getString(R.string.syserro));
            }
        });
    }

    /**
     * 接口响应成功
     * @return
     */
    private Response.Listener<BaseBean> listener() {
        return new Response.Listener<BaseBean>() {
            @Override
            public void onResponse(BaseBean baseBean) {
                dialog.dismiss();
                if (Constants.SUCCESSCODE.equals(baseBean.code)) {
                    setResult(RESULT_OK);
                    BoardEditActivity.this.finish();
                } else {
                    ToastUtils.showLongToast(BoardEditActivity.this, baseBean.message);
                }
            }
        };
    }

    @Override
    public void onClick(View view) {

        switch (view.getId()) {
            case R.id.title_bar_img_back:  //返回
                finish();
                break;
            case R.id.title_bar_txt_action:  //发布
                editNotice();
                break;
        }
    }
}
