package example.personal.connection.activity;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.Message;
import android.provider.MediaStore;
import android.text.TextUtils;
import android.view.View;
import android.widget.EditText;
import android.widget.HorizontalScrollView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.google.gson.Gson;

import java.io.File;

import example.personal.connection.Constants;
import example.personal.connection.MyApplication;
import example.personal.connection.R;
import example.personal.connection.Utils.CommonUtils;
import example.personal.connection.Utils.ToastUtils;
import example.personal.connection.entity.BaseBean;
import example.personal.connection.entity.ImageLoadResult;
import example.personal.connection.manager.ServiceManager;

/**
 * Created by wuqian on 2017/2/23.
 * mail: wuqian@ilingtong.com
 * Description:发朋友圈
 */
public class SendMonmentsActivity extends Activity implements View.OnClickListener {
    private ImageView titleBarImgBack;
    private TextView titleBarTxtTitle;
    private TextView titleBarTxtAction;
    private EditText sendMonmentsEditContent;
    private HorizontalScrollView sendMonmentsBanner;
    private LinearLayout llBanner;
    private ImageView sendMonmentsBtnAddimg;
    private String picPath = "";
    private final int REQUESTCODE_CHOOSE_PIC = 1002;
    private final int PHOTO_REQUEST_CAMERA = 1003;
    private final int REQUESTCODE_GET_PHOTO = 1001;
    private static final String PHOTO_FILE_NAME = "temp_photo.jpg";
    private File tempFile;
    private boolean flag = true;  //为true表示可以点击上传头像，否则不可上传头像
    private Bitmap bitmap;

    Handler handler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case 0:
                    Gson gson = new Gson();
                    ImageLoadResult imageLoadResult = gson.fromJson((String) msg.obj, ImageLoadResult.class);
                    if (Constants.SUCCESSCODE.equals(imageLoadResult.code)) {
                        picPath = imageLoadResult.data;
                        sendMonmentsBtnAddimg.setImageBitmap(bitmap);
                    } else {
                        ToastUtils.showLongToast(SendMonmentsActivity.this, imageLoadResult.message);
                    }
                    break;
                case 1:
                    Toast.makeText(getApplicationContext(), "服务器返回异常", Toast.LENGTH_LONG).show();
                    break;
            }
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_send_monments_layout);
        initView();
    }

    /**
     * 初始化view
     */
    private void initView() {
        titleBarImgBack = (ImageView) findViewById(R.id.title_bar_img_back);
        titleBarTxtTitle = (TextView) findViewById(R.id.title_bar_txt_title);
        titleBarTxtTitle.setVisibility(View.GONE);
        titleBarTxtAction = (TextView) findViewById(R.id.title_bar_txt_action);
        titleBarTxtAction.setText(getString(R.string.send));
        titleBarTxtAction.setVisibility(View.VISIBLE);
        sendMonmentsEditContent = (EditText) findViewById(R.id.send_monments_edit_content);
        sendMonmentsBanner = (HorizontalScrollView) findViewById(R.id.send_monments_banner);
        llBanner = (LinearLayout) findViewById(R.id.ll_banner);
        sendMonmentsBtnAddimg = (ImageView) findViewById(R.id.send_monments_btn_addimg);

        titleBarTxtAction.setOnClickListener(this);
        titleBarImgBack.setOnClickListener(this);
        sendMonmentsBtnAddimg.setOnClickListener(this);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == RESULT_OK) {
            switch (requestCode) {
                case REQUESTCODE_GET_PHOTO:  //拍照or选择照片
                    getPhoto(data.getIntExtra("type", 0));
                    break;
                case REQUESTCODE_CHOOSE_PIC:  //选择照片后处理
                    if (data != null) {
                        // 得到图片的全路径
                        Uri uri = data.getData();
                        bitmap = CommonUtils
                                .getSmallBitmap(CommonUtils.getRealPathFromURI(SendMonmentsActivity.this, uri), 480, 800);
                        uploadImage(bitmap);
                    }
                    break;
                case PHOTO_REQUEST_CAMERA:  //拍照后处理
                    if (hasSdcard()) {
                        tempFile = new File(Environment.getExternalStorageDirectory(),
                                PHOTO_FILE_NAME);
                        bitmap = CommonUtils.getSmallBitmap(tempFile.getAbsolutePath(), 480, 800);
                        uploadImage(bitmap);
                    } else {
                        ToastUtils.showLongToast(SendMonmentsActivity.this, "no sdcard!");
                    }
                    break;
            }
        }
    }

    /**
     * 选择获取图片方式后处理
     *
     * @param type
     */
    private void getPhoto(int type) {

        if (GetPhotoSelectDialogActivity.ACTION_SELECT_PIC == type) {
            Intent intent = new Intent(Intent.ACTION_PICK);
            intent.setType("image/*");
            startActivityForResult(intent, REQUESTCODE_CHOOSE_PIC);
        } else if (GetPhotoSelectDialogActivity.ACTION_TAKE_PIC == type) {
            camera();
        }
    }

    /**
     * 上传图片
     *
     * @param bitmap
     */
    private void uploadImage(final Bitmap bitmap) {
        new Thread() {
            @Override
            public void run() {
                flag = false;
                CommonUtils.uploadImage(Constants.IMAGE_UPLOAD, bitmap, handler);
                flag = true;
            }
        }.start();

    }

    /**
     * 启动相机拍照
     */
    public void camera() {
        Intent intent = new Intent("android.media.action.IMAGE_CAPTURE");
        // 判断存储卡是否可以用，可用进行存储
        if (hasSdcard()) {
            intent.putExtra(MediaStore.EXTRA_OUTPUT,
                    Uri.fromFile(new File(Environment
                            .getExternalStorageDirectory(), PHOTO_FILE_NAME)));
        }
        startActivityForResult(intent, PHOTO_REQUEST_CAMERA);
    }

    private boolean hasSdcard() {
        if (Environment.getExternalStorageState().equals(
                Environment.MEDIA_MOUNTED)) {
            return true;
        } else {
            return false;
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.title_bar_img_back:
                finish();
                break;
            case R.id.title_bar_txt_action:
                if (!TextUtils.isEmpty(sendMonmentsEditContent.getText().toString())) {
                    ServiceManager.sendMoment(MyApplication.getInstance().getUserId(), sendMonmentsEditContent.getText().toString(), picPath, listener(), new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            ToastUtils.showLongToast(SendMonmentsActivity.this, error.getMessage() + "");
                        }
                    });
                }
                break;
            case R.id.send_monments_btn_addimg:
                if (flag) {
                    GetPhotoSelectDialogActivity.launcher(this, REQUESTCODE_GET_PHOTO);
                }
                break;
        }
    }

    private Response.Listener<BaseBean> listener() {
        return new Response.Listener<BaseBean>() {
            @Override
            public void onResponse(BaseBean response) {
                if (Constants.SUCCESSCODE.equals(response.code)) {
                    ToastUtils.showLongToast(SendMonmentsActivity.this, "success");
                    SendMonmentsActivity.this.finish();
                } else {
                    ToastUtils.showLongToast(SendMonmentsActivity.this, response.message);
                }
            }
        };
    }
}
