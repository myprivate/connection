package example.personal.connection.activity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.nostra13.universalimageloader.core.ImageLoader;

import java.util.ArrayList;
import java.util.List;

import example.personal.connection.Constants;
import example.personal.connection.MyApplication;
import example.personal.connection.R;
import example.personal.connection.Utils.CommonUtils;
import example.personal.connection.Utils.ToastUtils;
import example.personal.connection.adapter.MomentsListAdapter;
import example.personal.connection.entity.MomentsResultBean;
import example.personal.connection.manager.ServiceManager;
import example.personal.connection.widget.XListView;

/**
 * Created by wuqian on 2017/3/3.
 * mail: wuqian@ilingtong.com
 * Description: 朋友圈帖子列表
 */
public class MomentsListActivity extends Activity implements XListView.IXListViewListener {

    private ImageView titleBarImgBack;
    private TextView titleBarTxtTitle;
    private XListView postListview;
    private TextView txtName;
    private ImageView imgHeadphoto;

    public List<MomentsResultBean.MomentsBean> list = new ArrayList<>();
    private int currentPageNum = 1;
    private MomentsListAdapter adapter;

    Handler handler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case 0:
                    postListview.setRefreshTime();
                    adapter.notifyDataSetChanged();
                    break;
            }
        }
    };

    public static void launch(Activity activity) {
        Intent intent = new Intent(activity, MomentsListActivity.class);
        activity.startActivity(intent);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mypost_layout);
        init();
        getMomentList(currentPageNum);
    }

    /**
     * 初始化view
     */
    private void init() {
        titleBarImgBack = (ImageView) findViewById(R.id.title_bar_img_back);
        titleBarImgBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                MomentsListActivity.this.finish();
            }
        });
        titleBarTxtTitle = (TextView) findViewById(R.id.title_bar_txt_title);
        titleBarTxtTitle.setText("Moments");
        postListview = (XListView) findViewById(R.id.post_listview);

        View headView = LayoutInflater.from(this).inflate(R.layout.list_headview_layout, null);
        txtName = (TextView) headView.findViewById(R.id.list_headview_txt_username);
        imgHeadphoto = (ImageView) headView.findViewById(R.id.list_headview_img_head);
        txtName.setText(MyApplication.getInstance().getUserInfo().name);
        if (!TextUtils.isEmpty(MyApplication.getInstance().getUserInfo().photo)) {
            ImageLoader.getInstance().displayImage(Constants.formatImageUrl(MyApplication.getInstance().getUserInfo().photo), imgHeadphoto, CommonUtils.getUserIconOptions());
        }
        postListview.addHeaderView(headView);
        adapter = new MomentsListAdapter(list, true, this);
        postListview.setPullLoadEnable(false);
        postListview.setXListViewListener(this,0);
        postListview.setAdapter(adapter);
    }


    /**
     * 获取朋友圈列表
     *
     * @param pageNum
     */
    private void getMomentList(int pageNum) {
        ServiceManager.getMoments(MyApplication.getInstance().getUserId(), Constants.PAGE_SIZE, pageNum, listener(), new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                ToastUtils.showLongToast(MomentsListActivity.this, error.getMessage() + "");
            }
        });
    }

    private Response.Listener<MomentsResultBean> listener() {
        return new Response.Listener<MomentsResultBean>() {
            @Override
            public void onResponse(MomentsResultBean response) {
                if (Constants.SUCCESSCODE.equals(response.code)) {
                    list.addAll(response.data.data);
                    handler.sendEmptyMessage(0);
                    currentPageNum = response.data.pageNum;
                    postListview.setPullLoadEnable(!response.data.lastPage);
                } else {
                    ToastUtils.showLongToast(MomentsListActivity.this, response.message);
                }
            }
        };
    }

    @Override
    public void onRefresh(int id) {
        currentPageNum = 1;
        list.clear();
        getMomentList(currentPageNum);
    }

    @Override
    public void onLoadMore(int id) {
        getMomentList(currentPageNum+1);
    }
}
