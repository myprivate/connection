package example.personal.connection.activity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.nostra13.universalimageloader.core.ImageLoader;

import java.util.ArrayList;
import java.util.List;

import example.personal.connection.Constants;
import example.personal.connection.MyApplication;
import example.personal.connection.R;
import example.personal.connection.Utils.CommonUtils;
import example.personal.connection.Utils.ToastUtils;
import example.personal.connection.adapter.MomentsListAdapter;
import example.personal.connection.entity.LoginInfoBean;
import example.personal.connection.entity.MomentsResultBean;
import example.personal.connection.manager.ServiceManager;
import example.personal.connection.widget.XListView;

/**
 * Created by wuqian on 2017/3/4.
 * mail: wuqian@ilingtong.com
 * Description: 我自己发布的朋友圈列表
 */
public class MyPostListActivity extends Activity implements XListView.IXListViewListener {
    private ImageView titleBarImgBack;
    private TextView titleBarTxtTitle;
    private XListView postListview;
    private TextView txtName;
    private ImageView imgHeadphoto;

    public List<MomentsResultBean.MomentsBean> list = new ArrayList<>();
    private int currentPageNum = 1;
    private MomentsListAdapter adapter;
    private LoginInfoBean userInfo;

    Handler handler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case 0:
                    postListview.stopRefresh();
                    postListview.stopLoadMore();
                    adapter.notifyDataSetChanged();
                    break;
            }
        }
    };

    public static void launch(Activity activity, LoginInfoBean userInfo) {
        Intent intent = new Intent(activity, MyPostListActivity.class);
        intent.putExtra("userInfo", userInfo);
        activity.startActivity(intent);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mypost_layout);
        userInfo = (LoginInfoBean) getIntent().getExtras().get("userInfo");
        init();
        getPostList(currentPageNum);
    }

    /**
     * 初始化view
     */
    private void init() {
        titleBarImgBack = (ImageView) findViewById(R.id.title_bar_img_back);
        titleBarImgBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                MyPostListActivity.this.finish();
            }
        });
        titleBarTxtTitle = (TextView) findViewById(R.id.title_bar_txt_title);
        titleBarTxtTitle.setText("MyPosts");
        postListview = (XListView) findViewById(R.id.post_listview);

        View headView = LayoutInflater.from(this).inflate(R.layout.list_headview_layout, null);
        txtName = (TextView) headView.findViewById(R.id.list_headview_txt_username);
        imgHeadphoto = (ImageView) headView.findViewById(R.id.list_headview_img_head);
        txtName.setText(userInfo.name);
        if (!TextUtils.isEmpty(userInfo.photo)) {
            ImageLoader.getInstance().displayImage(Constants.formatImageUrl(MyApplication.getInstance().getUserInfo().photo), imgHeadphoto, CommonUtils.getUserIconOptions());
        }
        postListview.addHeaderView(headView);
        adapter = new MomentsListAdapter(list, false, this);
        postListview.setPullLoadEnable(false);
        postListview.setXListViewListener(this, 0);
        postListview.setAdapter(adapter);
    }


    /**
     * 获取朋友圈列表
     *
     * @param pageNum
     */
    private void getPostList(int pageNum) {
        ServiceManager.getMyPostList(userInfo.id, Constants.PAGE_SIZE, pageNum, listener(), new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                ToastUtils.showLongToast(MyPostListActivity.this, error.getMessage() + "");
            }
        });
    }

    private Response.Listener<MomentsResultBean> listener() {
        return new Response.Listener<MomentsResultBean>() {
            @Override
            public void onResponse(MomentsResultBean response) {
                if (Constants.SUCCESSCODE.equals(response.code)) {
                    list.addAll(response.data.data);
                    handler.sendEmptyMessage(0);
                    currentPageNum = response.data.pageNum;
                    postListview.setPullLoadEnable(!response.data.lastPage);
                } else {
                    ToastUtils.showLongToast(MyPostListActivity.this, response.message);
                }
            }
        };
    }

    @Override
    public void onRefresh(int id) {
        currentPageNum = 1;
        list.clear();
        getPostList(currentPageNum);
    }

    @Override
    public void onLoadMore(int id) {
        getPostList(currentPageNum + 1);
    }
}
