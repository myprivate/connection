package example.personal.connection.activity;

import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.LinearLayout;
import android.widget.TextView;



import example.personal.connection.R;
import example.personal.connection.Utils.ToastUtils;

/**
 * Created by wuqian on 2016/10/12.
 * mail: wuqian@ilingtong.com
 * Description:拍照/选择照片dialog
 */
public class GetPhotoSelectDialogActivity extends Activity implements View.OnClickListener {
    TextView txtCamera;
    TextView txtPhotos;
    TextView txtCancel;
    private static final int MY_PERMISSIONS_REQUEST_CAMERA = 1002;
    private SelectDialog mTipsDialog;

    public static final int ACTION_TAKE_PIC = 0;
    public static final int ACTION_SELECT_PIC = 1;


    /**
     * 显示选择拍照or相册获取dialog
     *
     * @param activity
     * @param requestCode
     */
    public static void launcher(Activity activity, int requestCode) {
        Intent intent = new Intent(activity, GetPhotoSelectDialogActivity.class);
        activity.startActivityForResult(intent, requestCode);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        Window window = this.getWindow();
        setContentView(R.layout.dialog_getpic_layout);
        window.getDecorView().setPadding(0, 0, 0, 0);
        WindowManager.LayoutParams params = getWindow().getAttributes();
        params.width = LinearLayout.LayoutParams.MATCH_PARENT;
        params.height = LinearLayout.LayoutParams.WRAP_CONTENT;
        params.gravity = Gravity.BOTTOM;
        window.setAttributes(params);
        init();
    }

    /**
     * 初始化
     */
    private void init() {
        txtCamera = (TextView) findViewById(R.id.dialog_getpic_txt_camera);
        txtPhotos = (TextView) findViewById(R.id.dialog_getpic_txt_photos);
        txtCancel = (TextView) findViewById(R.id.dialog_getpic_txt_cancel);

        txtCancel.setOnClickListener(this);
        txtCamera.setOnClickListener(this);
        txtPhotos.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {

        switch (view.getId()) {
            case R.id.dialog_getpic_txt_cancel:  //取消
                this.finish();
                break;
            case R.id.dialog_getpic_txt_photos:  //从相册中选择照片
                Intent intent = new Intent();
                intent.putExtra("type", ACTION_SELECT_PIC);
                setResult(RESULT_OK, intent);
                finish();
                break;
            case R.id.dialog_getpic_txt_camera:  //拍照
                if (ContextCompat.checkSelfPermission(GetPhotoSelectDialogActivity.this,
                        Manifest.permission.CAMERA)
                        != PackageManager.PERMISSION_GRANTED || ContextCompat.checkSelfPermission(GetPhotoSelectDialogActivity.this,
                        Manifest.permission.WRITE_EXTERNAL_STORAGE)
                        != PackageManager.PERMISSION_GRANTED) {

                    if (ActivityCompat.shouldShowRequestPermissionRationale(GetPhotoSelectDialogActivity.this,
                            Manifest.permission.CAMERA) || ActivityCompat.shouldShowRequestPermissionRationale(GetPhotoSelectDialogActivity.this, Manifest.permission.WRITE_EXTERNAL_STORAGE)) {

                        // sees the explanation, try again to request the permission.
                        mTipsDialog = new SelectDialog(GetPhotoSelectDialogActivity.this, null, getString(R.string.kown), getString(R.string.camera_permission_tips), null, new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                mTipsDialog.dismiss();
                                ActivityCompat.requestPermissions(GetPhotoSelectDialogActivity.this,
                                        new String[]{Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE},
                                        MY_PERMISSIONS_REQUEST_CAMERA);
                            }
                        });
                        mTipsDialog.show();

                    } else {
                        //request permissions
                        ActivityCompat.requestPermissions(GetPhotoSelectDialogActivity.this,
                                new String[]{Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE},
                                MY_PERMISSIONS_REQUEST_CAMERA);
                    }
                } else {
                    Intent intent1 = new Intent();
                    intent1.putExtra("type", ACTION_TAKE_PIC);
                    setResult(RESULT_OK, intent1);
                    finish();
                }
                break;
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST_CAMERA: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED && grantResults[1] == PackageManager.PERMISSION_GRANTED) {
                    Intent intent1 = new Intent();
                    intent1.putExtra("type", ACTION_TAKE_PIC);
                    setResult(RESULT_OK, intent1);
                    finish();
                } else {
                    ToastUtils.showLongToast(GetPhotoSelectDialogActivity.this, getString(R.string.please_open_the_camera_permission));
                }
                return;
            }
        }
    }
}
