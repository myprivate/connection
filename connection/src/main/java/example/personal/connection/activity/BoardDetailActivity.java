package example.personal.connection.activity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import java.io.Serializable;

import example.personal.connection.R;
import example.personal.connection.entity.BoardListResult;

/**
 * Created by wuqian on 2017/2/23.
 * mail: wuqian@ilingtong.com
 * Description:公告详情
 */
public class BoardDetailActivity extends Activity {
    private ImageView titleBarImgBack;
    private TextView titleBarTxtTitle;
    private TextView titleBarTxtAction;
    private TextView boardDetailTitle;
    private TextView boardDetailContent;
    private TextView boardDetailTime;
    private BoardListResult.BoardBean boardInfo;


    public static void launch(Activity activity, BoardListResult.BoardBean boardBean){
        Intent intent = new Intent(activity,BoardDetailActivity.class);
        intent.putExtra("boardInfo", (Serializable) boardBean);
        activity.startActivity(intent);
    }
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_board_detail_layout);
        boardInfo = (BoardListResult.BoardBean) getIntent().getExtras().get("boardInfo");
        initView();
    }

    /**
     * 初始化view
     */
    private void initView() {
        titleBarImgBack = (ImageView) findViewById(R.id.title_bar_img_back);
        titleBarImgBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
        titleBarTxtTitle = (TextView) findViewById(R.id.title_bar_txt_title);
        titleBarTxtTitle.setText(getString(R.string.bulletinBoard));
        titleBarTxtAction = (TextView) findViewById(R.id.title_bar_txt_action);
        titleBarTxtAction.setVisibility(View.GONE);
        boardDetailTitle = (TextView) findViewById(R.id.board_detail_title);
        boardDetailContent = (TextView) findViewById(R.id.board_detail_content);
        boardDetailTime = (TextView) findViewById(R.id.board_detail_time);

        boardDetailTitle.setText(boardInfo.title);
        boardDetailContent.setText(boardInfo.content);
        boardDetailTime.setText(boardInfo.createTime);
    }
}
