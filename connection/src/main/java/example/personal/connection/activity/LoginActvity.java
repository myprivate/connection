package example.personal.connection.activity;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Response;
import com.android.volley.VolleyError;

import example.personal.connection.Constants;
import example.personal.connection.MyApplication;
import example.personal.connection.R;
import example.personal.connection.entity.LoginResultBean;
import example.personal.connection.manager.ServiceManager;
import example.personal.connection.module.emchat.EMChatManager;

/**
 * Created by wuqian on 2017/2/22.
 * mail: wuqian@ilingtong.com
 * Description:
 */
public class LoginActvity extends Activity implements View.OnClickListener {
    private ImageView titleBarImgBack;
    private TextView titleBarTxtTitle;
    private TextView titleBarTxtAction;
    private EditText loginEditPhone;
    private EditText loginEditPassword;
    private Button loginBtnLogin;
    private TextView loginTxtToregister;
    private SharedPreferences sp;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login_layout);
        sp = getSharedPreferences("login", Context.MODE_PRIVATE);
        initView();
    }

    /**
     * 初始化view
     */
    private void initView() {
        titleBarImgBack = (ImageView) findViewById(R.id.title_bar_img_back);
        titleBarTxtTitle = (TextView) findViewById(R.id.title_bar_txt_title);
        titleBarTxtTitle.setText(getString(R.string.login));
        titleBarTxtAction = (TextView) findViewById(R.id.title_bar_txt_action);
        titleBarTxtAction.setVisibility(View.GONE);
        loginEditPhone = (EditText) findViewById(R.id.login_edit_phone);
        loginEditPassword = (EditText) findViewById(R.id.login_edit_password);
        loginBtnLogin = (Button) findViewById(R.id.login_btn_login);
        loginTxtToregister = (TextView) findViewById(R.id.login_txt_toregister);

        loginEditPhone.setText(sp.getString("username",""));
        loginEditPassword.setText(sp.getString("password",""));
        loginEditPhone.setSelection(loginEditPhone.length());
        loginEditPassword.setSelection(loginEditPassword.length());

        titleBarImgBack.setOnClickListener(this);
        loginBtnLogin.setOnClickListener(this);
        loginTxtToregister.setOnClickListener(this);
    }

    /**
     * 登录
     */
    private void doLogin() {

        final String phone = loginEditPhone.getText().toString();
        final String pwd = loginEditPassword.getText().toString();
        ServiceManager.doLogin(phone, pwd, new Response.Listener<LoginResultBean>() {
            @Override
            public void onResponse(LoginResultBean response) {

                if (Constants.SUCCESSCODE.equals(response.code)) {
                    MyApplication.getInstance().setUserInfo(response.data);
                    String voipUsername = response.data.voipUsername;
                    String voipPwd = response.data.voipPwd;
                    if(!TextUtils.isEmpty(voipUsername)) {
                        EMChatManager.getInstance().syncLoginOutEM();
                        EMChatManager.getInstance().loginEM(voipUsername,voipPwd);
                    }
                    Intent intent = new Intent(LoginActvity.this, MainFragmentActivity.class);
                    startActivity(intent);
                    sp.edit().putString("username",phone).commit();
                    sp.edit().putString("password",pwd).commit();
                    LoginActvity.this.finish();
                } else {
                    Toast.makeText(LoginActvity.this, response.message, Toast.LENGTH_LONG).show();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(LoginActvity.this, getString(R.string.syserro), Toast.LENGTH_LONG).show();
            }
        });

    }


    @Override
    public void onClick(View view) {

        switch (view.getId()) {
            case R.id.title_bar_img_back:
                finish();
                break;
            case R.id.login_btn_login:
                doLogin();
                break;
            case R.id.login_txt_toregister:
                Intent intent = new Intent(LoginActvity.this, RegisterActivity.class);
                startActivity(intent);
                break;
        }
    }

}
