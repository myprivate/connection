package example.personal.connection.activity;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.Message;
import android.provider.MediaStore;
import android.text.TextUtils;
import android.view.Gravity;
import android.view.View;
import android.widget.DatePicker;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.NumberPicker;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.google.gson.Gson;
import com.nostra13.universalimageloader.core.ImageLoader;

import java.io.File;
import java.util.Calendar;

import example.personal.connection.Constants;
import example.personal.connection.MyApplication;
import example.personal.connection.R;
import example.personal.connection.Utils.CommonUtils;
import example.personal.connection.Utils.DialogUtils;
import example.personal.connection.Utils.ToastUtils;
import example.personal.connection.entity.BaseBean;
import example.personal.connection.entity.ImageLoadResult;
import example.personal.connection.entity.LoginInfoBean;
import example.personal.connection.entity.LoginResultBean;
import example.personal.connection.entity.RequestUserInfoBean;
import example.personal.connection.manager.ServiceManager;

/**
 * Created by wuqian on 2017/3/2.
 * mail: wuqian@ilingtong.com
 * Description: 我的个人信息
 */
public class MyInfomationActivity extends Activity implements View.OnClickListener {
    private ImageView titleBarImgBack;
    private TextView titleBarTxtTitle;
    private RelativeLayout myinfoLlHead;
    private ImageView myinfoImgHead;
    private LinearLayout myinfoLlName;
    private TextView myinfoTxtName;
    private TextView myinfoTxtPhone;
    private LinearLayout myinfoLlBirthday;
    private TextView myinfoTxtBirthday;
    private LinearLayout myinfoLlEmail;
    private TextView myinfoTxtEmail;
    private LinearLayout myinfoLlCity;
    private TextView myinfoTxtCity;
    private LinearLayout myinfoLlUniversity;
    private TextView myinfoTxtUniversity;
    private LinearLayout myinfoLlYearOfGraduation;
    private TextView myinfoTxtYearOfGraduation;
    private LinearLayout myinfoLlMajor;
    private TextView myinfoTxtMajor;
    private LinearLayout myinfoLlClassNumber;
    private TextView myinfoTxtClassNumber;
    private Dialog dialog;
    private LoginInfoBean userInfo;
    private RequestUserInfoBean param = new RequestUserInfoBean();
    private boolean flag = true;  //为true表示可以点击上传头像，否则不可上传头像
    private File tempFile;
    private Bitmap headBitmap;

    Handler handler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case 0:
                    Gson gson = new Gson();
                    ImageLoadResult imageLoadResult = gson.fromJson((String) msg.obj, ImageLoadResult.class);
                    if (Constants.SUCCESSCODE.equals(imageLoadResult.code)) {
                        param.photo = imageLoadResult.data;
                        ServiceManager.updateUserInfo(param, updateSuccessListener(), errorListener);
                    } else {
                        ToastUtils.showLongToast(MyInfomationActivity.this, imageLoadResult.message);
                    }
                    break;
                case 1:
                    Toast.makeText(getApplicationContext(), "服务器返回异常", Toast.LENGTH_LONG).show();
                    break;
            }
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_infomation);
        init();
        getData();
    }

    private void init() {
        dialog = DialogUtils.createLoadingDialog(this);
        titleBarImgBack = (ImageView) findViewById(R.id.title_bar_img_back);
        titleBarImgBack.setOnClickListener(this);
        titleBarTxtTitle = (TextView) findViewById(R.id.title_bar_txt_title);
        titleBarTxtTitle.setText("Me");

        myinfoLlHead = (RelativeLayout) findViewById(R.id.myinfo_ll_head);
        myinfoLlHead.setOnClickListener(this);
        myinfoImgHead = (ImageView) findViewById(R.id.myinfo_img_head);
        myinfoLlName = (LinearLayout) findViewById(R.id.myinfo_ll_name);
        myinfoLlName.setOnClickListener(this);
        myinfoTxtName = (TextView) findViewById(R.id.myinfo_txt_name);
        myinfoTxtPhone = (TextView) findViewById(R.id.myinfo_txt_phone);
        myinfoLlBirthday = (LinearLayout) findViewById(R.id.myinfo_ll_birthday);
        myinfoLlBirthday.setOnClickListener(this);
        myinfoTxtBirthday = (TextView) findViewById(R.id.myinfo_txt_birthday);
        myinfoLlEmail = (LinearLayout) findViewById(R.id.myinfo_ll_email);
        myinfoLlEmail.setOnClickListener(this);
        myinfoTxtEmail = (TextView) findViewById(R.id.myinfo_txt_email);
        myinfoLlCity = (LinearLayout) findViewById(R.id.myinfo_ll_city);
        myinfoLlCity.setOnClickListener(this);
        myinfoTxtCity = (TextView) findViewById(R.id.myinfo_txt_city);
        myinfoLlUniversity = (LinearLayout) findViewById(R.id.myinfo_ll_university);
        myinfoLlUniversity.setOnClickListener(this);
        myinfoTxtUniversity = (TextView) findViewById(R.id.myinfo_txt_university);
        myinfoLlYearOfGraduation = (LinearLayout) findViewById(R.id.myinfo_ll_year_of_graduation);
        myinfoLlYearOfGraduation.setOnClickListener(this);
        myinfoTxtYearOfGraduation = (TextView) findViewById(R.id.myinfo_txt_year_of_graduation);
        myinfoLlMajor = (LinearLayout) findViewById(R.id.myinfo_ll_major);
        myinfoLlMajor.setOnClickListener(this);
        myinfoTxtMajor = (TextView) findViewById(R.id.myinfo_txt_major);
        myinfoLlClassNumber = (LinearLayout) findViewById(R.id.myinfo_ll_class_number);
        myinfoLlClassNumber.setOnClickListener(this);
        myinfoTxtClassNumber = (TextView) findViewById(R.id.myinfo_txt_class_number);
    }

    /**
     * 获取我的信息
     */
    private void getData() {
        dialog.show();
        ServiceManager.getUserInfo(MyApplication.getInstance().getUserId(), listener(), errorListener);
    }

    private Response.ErrorListener errorListener = new Response.ErrorListener() {
        @Override
        public void onErrorResponse(VolleyError error) {
            dialog.dismiss();
            ToastUtils.showLongToast(MyInfomationActivity.this, error.getMessage());
        }
    };


    private Response.Listener<LoginResultBean> listener() {
        return new Response.Listener<LoginResultBean>() {
            @Override
            public void onResponse(LoginResultBean response) {
                dialog.dismiss();
                if (Constants.SUCCESSCODE.equals(response.code)) {
                    userInfo = response.data;
                    setView(response.data);
                } else {
                    ToastUtils.showLongToast(MyInfomationActivity.this, response.message);
                }
            }
        };
    }

    /**
     * 赋值
     *
     * @param userInfo
     */
    private void setView(LoginInfoBean userInfo) {

        MyApplication.getInstance().setUserInfo(userInfo);

        if (!TextUtils.isEmpty(userInfo.photo)) {
            ImageLoader.getInstance().displayImage(Constants.formatImageUrl(userInfo.photo), myinfoImgHead, CommonUtils.getUserIconOptions());
        }
        myinfoTxtName.setText(userInfo.name + "");
        myinfoTxtPhone.setText(userInfo.phone + "");
        myinfoTxtBirthday.setText(userInfo.birthday + "");
        myinfoTxtEmail.setText(userInfo.email + "");
        myinfoTxtCity.setText(userInfo.city + "");
        myinfoTxtUniversity.setText(userInfo.university + "");
        myinfoTxtYearOfGraduation.setText(userInfo.yearOfGraduation + "");
        myinfoTxtMajor.setText(userInfo.major + "");
        myinfoTxtClassNumber.setText(userInfo.classNumber + "");

        param.name = userInfo.name;
        param.birthday = userInfo.birthday;
        param.email = userInfo.email;
        param.city = userInfo.city;
        param.university = userInfo.university;
        param.yearOfGraduation = userInfo.yearOfGraduation;
        param.major = userInfo.major;
        param.classNumber = userInfo.classNumber;
        param.photo = userInfo.photo;
        param.phone = userInfo.phone;
    }

    public final static int REQUEST_NAME = 1000;
    public final static int REQUEST_GET_PHOTO = 1001;
    public final static int REQUEST_EMAIL = 1002;
    public final static int REQUEST_CITY = 1003;
    public final static int REQUEST_UNIVERSITY = 1004;
    public final static int REQUEST_MAJOR = 1005;
    public final static int REQUEST_CLASS = 1006;
    private final int REQUESTCODE_CHOOSE_PIC = 1102;
    private final int PHOTO_REQUEST_CAMERA = 1103;
    private static final String PHOTO_FILE_NAME = "temp_photo.jpg";
    private final int REQUEST_CROP_IMG = 1104;


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == RESULT_OK) {
            String text = "";
            if (data != null ) {
                text = data.getStringExtra("text");
            }
            switch (requestCode) {
                case REQUEST_NAME:
                    userInfo.name = text;
                    setView(userInfo);
                    break;
                case REQUEST_EMAIL:
                    userInfo.email = text;
                    setView(userInfo);
                    break;
                case REQUEST_CITY:
                    userInfo.city = text;
                    setView(userInfo);
                    break;
                case REQUEST_UNIVERSITY:
                    userInfo.university = text;
                    setView(userInfo);
                    break;
                case REQUEST_MAJOR:
                    userInfo.major = text;
                    setView(userInfo);
                    break;
                case REQUEST_CLASS:
                    userInfo.classNumber = text;
                    setView(userInfo);
                    break;
                case REQUEST_GET_PHOTO:  //拍照or选择照片
                    setHeadIcon(data.getIntExtra("type", 0));
                    break;
                case REQUESTCODE_CHOOSE_PIC:  //选择照片后处理
                    if (data != null) {
                        // 得到图片的全路径
                        Uri uri = data.getData();
                        crop(uri);
                    }
                    break;
                case PHOTO_REQUEST_CAMERA:  //拍照后处理
                    if (hasSdcard()) {
                        tempFile = new File(Environment.getExternalStorageDirectory(),
                                PHOTO_FILE_NAME);
                        crop(Uri.fromFile(tempFile));
                    } else {
                        ToastUtils.showLongToast(MyInfomationActivity.this, "no sdcard!");
                    }
                    break;
                case REQUEST_CROP_IMG:  //裁剪后处理
                    try {
                        headBitmap = data.getParcelableExtra("data");
                        new Thread() {
                            @Override
                            public void run() {
                                flag = false;
                                CommonUtils.uploadImage(Constants.IMAGE_UPLOAD, headBitmap, handler);
                                flag = true;
                            }
                        }.start();

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    break;
            }
        }
    }

    /**
     * 选择获取图片方式后处理
     *
     * @param type
     */
    private void setHeadIcon(int type) {

        if (GetPhotoSelectDialogActivity.ACTION_SELECT_PIC == type) {
            Intent intent = new Intent(Intent.ACTION_PICK);
            intent.setType("image/*");
            startActivityForResult(intent, REQUESTCODE_CHOOSE_PIC);
        } else if (GetPhotoSelectDialogActivity.ACTION_TAKE_PIC == type) {
            camera();
        }
    }

    /**
     * 启动相机拍照
     */
    public void camera() {
        Intent intent = new Intent("android.media.action.IMAGE_CAPTURE");
        // 判断存储卡是否可以用，可用进行存储
        if (hasSdcard()) {
            intent.putExtra(MediaStore.EXTRA_OUTPUT,
                    Uri.fromFile(new File(Environment
                            .getExternalStorageDirectory(), PHOTO_FILE_NAME)));
        }
        startActivityForResult(intent, PHOTO_REQUEST_CAMERA);
    }

    private boolean hasSdcard() {
        if (Environment.getExternalStorageState().equals(
                Environment.MEDIA_MOUNTED)) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * 剪切头像图片
     *
     * @param uri
     * @function:
     * @author:Jerry
     * @date:2013-12-30
     */
    private void crop(Uri uri) {
        // 裁剪图片意图
        Intent intent = new Intent("com.android.camera.action.CROP");
        intent.setDataAndType(uri, "image/*");
        intent.putExtra("crop", "true");
        // 裁剪框的比例，1：1
        intent.putExtra("aspectX", 1);
        intent.putExtra("aspectY", 1);
        // 裁剪后输出图片的尺寸大小
        intent.putExtra("outputX", 280);
        intent.putExtra("outputY", 280);
        // 图片格式
        intent.putExtra("outputFormat", "JPEG");
        intent.putExtra("noFaceDetection", true);// 取消人脸识别
        intent.putExtra("return-data", true);// true:不返回uri，false：返回uri
        startActivityForResult(intent, REQUEST_CROP_IMG);
    }

    @Override
    public void onClick(View view) {

        switch (view.getId()) {
            case R.id.title_bar_img_back:
                this.finish();
                break;
            case R.id.myinfo_ll_head:
                if (flag) {
                    GetPhotoSelectDialogActivity.launcher(this, REQUEST_GET_PHOTO);
                }
                break;
            case R.id.myinfo_ll_birthday:
                showDatePickerDialog();
                break;
            case R.id.myinfo_ll_email:
                UpdateMyInfoActivity.launcher(this, param, param.email, REQUEST_EMAIL);
                break;
            case R.id.myinfo_ll_city:
                UpdateMyInfoActivity.launcher(this, param, param.city, REQUEST_CITY);
                break;
            case R.id.myinfo_ll_university:
                UpdateMyInfoActivity.launcher(this, param, param.university, REQUEST_UNIVERSITY);
                break;
            case R.id.myinfo_ll_year_of_graduation:
                createAlertNumberPicker();
                break;
            case R.id.myinfo_ll_major:
                UpdateMyInfoActivity.launcher(this, param, param.major, REQUEST_MAJOR);
                break;
            case R.id.myinfo_ll_class_number:
                UpdateMyInfoActivity.launcher(this, param, param.classNumber, REQUEST_CLASS);
                break;
            case R.id.myinfo_ll_name:
                UpdateMyInfoActivity.launcher(this, param, param.name, REQUEST_NAME);
                break;
        }
    }

    /**
     * 弹出日历选择器并修改生日
     */
    private void showDatePickerDialog() {
        Calendar calendar = Calendar.getInstance();
        DatePickerDialog datePickerDialog = new DatePickerDialog(MyInfomationActivity.this, new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker datePicker, int year, int month, int dayofmonth) {
                String date = year + "/" + (month + 1) + "/" + dayofmonth;
                param.birthday = date;
                ServiceManager.updateUserInfo(param, updateSuccessListener(), errorListener);
            }
        }
                , calendar.get(Calendar.YEAR)
                , calendar.get(Calendar.MONTH)
                , calendar.get(Calendar.DAY_OF_MONTH));

        Calendar maxCalendar = Calendar.getInstance();
        datePickerDialog.getDatePicker().setMaxDate(maxCalendar.getTimeInMillis());
        datePickerDialog.show();
    }

    private Response.Listener<BaseBean> updateSuccessListener() {
        return new Response.Listener<BaseBean>() {
            @Override
            public void onResponse(BaseBean response) {
                if (Constants.SUCCESSCODE.equals(response.code)) {
                    myinfoTxtBirthday.setText(param.birthday);
                    myinfoTxtYearOfGraduation.setText(param.yearOfGraduation);
                    userInfo.birthday = param.birthday;
                    userInfo.yearOfGraduation = param.yearOfGraduation;
                    if (headBitmap != null) {
                        myinfoImgHead.setImageBitmap(headBitmap);
                        userInfo.photo = param.photo;
                    }
                    MyApplication.getInstance().setUserInfo(userInfo);
                }
            }
        };
    }

    /**
     * 显示毕业年份选择器
     */
    private void createAlertNumberPicker() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        final NumberPicker numberPicker = new NumberPicker(this);
        Calendar calendar = Calendar.getInstance();
        numberPicker.setMaxValue(calendar.get(Calendar.YEAR));
        numberPicker.setMinValue(calendar.get(Calendar.YEAR) - 100);
        numberPicker.setValue(calendar.get(Calendar.YEAR));
        numberPicker.setOnValueChangedListener(new NumberPicker.OnValueChangeListener() {
            @Override
            public void onValueChange(NumberPicker numberPicker, int oldNum, int newNum) {
                param.yearOfGraduation = newNum + "";
            }
        });
        final FrameLayout parent = new FrameLayout(this);
        parent.addView(numberPicker, new FrameLayout.LayoutParams(
                FrameLayout.LayoutParams.WRAP_CONTENT,
                FrameLayout.LayoutParams.WRAP_CONTENT,
                Gravity.CENTER));
        builder.setView(parent);
        builder.setTitle(getString(R.string.year_of_graduation))
                .setCancelable(false)
                .setPositiveButton(getString(R.string.ok), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        ServiceManager.updateUserInfo(param, updateSuccessListener(), errorListener);
                    }
                });
        AlertDialog alertDialog = builder.create();
        alertDialog.show();
    }
}
