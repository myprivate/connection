package example.personal.connection.activity;

import android.app.Activity;
import android.app.Dialog;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.volley.Response;
import com.android.volley.VolleyError;

import java.util.ArrayList;
import java.util.List;

import example.personal.connection.Constants;
import example.personal.connection.MyApplication;
import example.personal.connection.R;
import example.personal.connection.Utils.DialogUtils;
import example.personal.connection.Utils.ToastUtils;
import example.personal.connection.adapter.FriendApplyListAdapter;
import example.personal.connection.entity.BaseBean;
import example.personal.connection.entity.FriendApplyBean;
import example.personal.connection.entity.FriendApplyListBean;
import example.personal.connection.manager.ServiceManager;
import example.personal.connection.widget.XListView;

/**
 * Created by wuqian on 2017/3/4.
 * mail: wuqian@ilingtong.com
 * Description: 好友申请列表
 */
public class FriendApplyListActivity extends Activity {
    private ImageView titleBarImgBack;
    private TextView titleBarTxtTitle;
    private XListView searchListUsers;

    public final static int WHAT_AGREEN = 1;
    public final static int WHAT_SUCESS = 2;
    private final int REFRESH_LISTVIEW = 3;
    private FriendApplyListAdapter adapter;
    public List<FriendApplyBean> list = new ArrayList<>();
    private int position;
    private Dialog dialog;
    private boolean updateFlag = false;

    Handler handler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case WHAT_AGREEN:   //发起好友申请请求
                    position = msg.arg1;
                    agreen(list.get(position).id);
                    break;
                case WHAT_SUCESS:  //同意添加好友成功
                    ToastUtils.showLongToast(FriendApplyListActivity.this,"Success");
                    list.remove(position);
                    if (list.size()<1){
                        setResult(RESULT_OK);
                        finish();
                    }else {
                        adapter.notifyDataSetChanged();
                        updateFlag = true;  //表示同意了好友申请，回到校友列表时要刷新校友列表
                    }
                    break;
                case REFRESH_LISTVIEW:
                    adapter.notifyDataSetChanged();
            }
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search_layout);
        init();
        getApplyList();
    }

    private void init() {
        dialog = DialogUtils.createLoadingDialog(this);
        titleBarImgBack = (ImageView) findViewById(R.id.title_bar_img_back);
        titleBarImgBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                FriendApplyListActivity.this.finish();
            }
        });
        titleBarTxtTitle = (TextView) findViewById(R.id.title_bar_txt_title);
        titleBarTxtTitle.setText("New Friend");
        findViewById(R.id.alumni_sv_search).setVisibility(View.GONE);
        searchListUsers = (XListView) findViewById(R.id.search_list_users);
        searchListUsers.setPullLoadEnable(false);
        searchListUsers.setPullRefreshEnable(false);

        adapter = new FriendApplyListAdapter(list, handler, this);
        searchListUsers.setAdapter(adapter);
    }

    /**
     * 同意添加好友
     *
     * @param id
     */
    private void agreen(String id) {
        ServiceManager.agreeApply(id, listener(), errorListener());
    }

    private Response.Listener<BaseBean> listener() {
        return new Response.Listener<BaseBean>() {
            @Override
            public void onResponse(BaseBean response) {
                if (Constants.SUCCESSCODE.equals(response.code)) {
                    handler.sendEmptyMessage(WHAT_SUCESS);
                } else {
                    ToastUtils.showLongToast(FriendApplyListActivity.this, response.message);
                }
            }
        };
    }

    private Response.ErrorListener errorListener() {
        return new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                dialog.dismiss();
                ToastUtils.showLongToast(FriendApplyListActivity.this, getString(R.string.syserro));
            }
        };
    }

    /**
     * 获取好友申请列表
     */
    private void getApplyList() {
        dialog.show();
        ServiceManager.getApplyList(MyApplication.getInstance().getUserId(),successListener(),errorListener());
    }

    private Response.Listener<FriendApplyListBean> successListener() {
        return new Response.Listener<FriendApplyListBean>() {
            @Override
            public void onResponse(FriendApplyListBean response) {
                dialog.dismiss();
                if (Constants.SUCCESSCODE.equals(response.code)){
                    list.addAll(response.data);
                    handler.sendEmptyMessage(REFRESH_LISTVIEW);
                }else {
                    ToastUtils.showLongToast(FriendApplyListActivity.this,response.message);
                }
            }
        };
    }

    @Override
    public void finish() {
        if (updateFlag){
            setResult(RESULT_OK);
        }
        super.finish();
    }
}
