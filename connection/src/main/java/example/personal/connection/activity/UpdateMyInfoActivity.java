package example.personal.connection.activity;

import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.volley.Response;
import com.android.volley.VolleyError;

import java.io.Serializable;

import example.personal.connection.Constants;
import example.personal.connection.MyApplication;
import example.personal.connection.R;
import example.personal.connection.Utils.DialogUtils;
import example.personal.connection.Utils.ToastUtils;
import example.personal.connection.entity.BaseBean;
import example.personal.connection.entity.RequestUserInfoBean;
import example.personal.connection.manager.ServiceManager;

/**
 * Created by wuqian on 2017/3/2.
 * mail: wuqian@ilingtong.com
 * Description: 修改个人信息
 */
public class UpdateMyInfoActivity extends Activity implements View.OnClickListener {
    private ImageView titleBarImgBack;
    private TextView titleBarTxtTitle;
    private TextView titleBarTxtAction;
    private EditText updateEditContent;
    private Dialog dialog;
    private RequestUserInfoBean param;
    private int requestcode;
    private String text;


    public static void launcher(Activity activity, RequestUserInfoBean param, String text, int requestcode) {

        Intent intent = new Intent(activity, UpdateMyInfoActivity.class);
        intent.putExtra("param", (Serializable) param);
        intent.putExtra("text",text);
        intent.putExtra("requestcode",requestcode);
        activity.startActivityForResult(intent,requestcode);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_update_info_layout);
        param = (RequestUserInfoBean) getIntent().getExtras().get("param");
        text = getIntent().getStringExtra("text");
        requestcode = getIntent().getIntExtra("requestcode",0);
        init();
    }

    private void init() {

        dialog = DialogUtils.createLoadingDialog(this);
        titleBarImgBack = (ImageView) findViewById(R.id.title_bar_img_back);
        titleBarImgBack.setVisibility(View.GONE);
        titleBarTxtTitle = (TextView) findViewById(R.id.title_bar_txt_title);
        titleBarTxtTitle.setText(getString(R.string.cancel));
        titleBarTxtTitle.setOnClickListener(this);
        titleBarTxtAction = (TextView) findViewById(R.id.title_bar_txt_action);
        titleBarTxtAction.setVisibility(View.VISIBLE);
        titleBarTxtAction.setOnClickListener(this);
        titleBarTxtAction.setEnabled(false);
        updateEditContent = (EditText) findViewById(R.id.update_edit_content);
        updateEditContent.setText(text);
        updateEditContent.setSelection(updateEditContent.getText().length());
        updateEditContent.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int count) {
                if (charSequence.length() > 0) {
                    titleBarTxtAction.setEnabled(true);
                } else {
                    titleBarTxtAction.setEnabled(false);
                }
                text = charSequence.toString();
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });
    }

    @Override
    public void onClick(View view) {

        switch (view.getId()) {
            case R.id.title_bar_txt_title:
                finish();
                break;
            case R.id.title_bar_txt_action:
                doUpdate();
                break;
        }
    }

    /**
     * 调用接口修改
     */
    private void doUpdate() {
        dialog.show();
        switch (requestcode) {
            case MyInfomationActivity.REQUEST_NAME:
                param.name = updateEditContent.getText().toString();
                break;
            case MyInfomationActivity.REQUEST_EMAIL:
                param.email = updateEditContent.getText().toString();
                break;
            case MyInfomationActivity.REQUEST_CITY:
                param.city = updateEditContent.getText().toString();
                break;
            case MyInfomationActivity.REQUEST_UNIVERSITY:
                param.university = updateEditContent.getText().toString();
                break;
            case MyInfomationActivity.REQUEST_MAJOR:
                param.major = updateEditContent.getText().toString();
                break;
            case MyInfomationActivity.REQUEST_CLASS:
                param.classNumber = updateEditContent.getText().toString();
                break;
        }
        ServiceManager.updateUserInfo(param, listener(), new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                dialog.dismiss();
                ToastUtils.showLongToast(UpdateMyInfoActivity.this, error.getMessage());
            }
        });
    }

    private Response.Listener<BaseBean> listener() {
        return new Response.Listener<BaseBean>() {
            @Override
            public void onResponse(BaseBean response) {
                if (Constants.SUCCESSCODE.equals(response.code)) {
                    dialog.dismiss();
                    Intent intent = new Intent();
                    intent.putExtra("text", updateEditContent.getText().toString());
                    setResult(RESULT_OK, intent);
                    UpdateMyInfoActivity.this.finish();
                }
            }
        };
    }
}
