package example.personal.connection.activity;

import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.widget.SwipeRefreshLayout;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.hyphenate.EMMessageListener;
import com.hyphenate.chat.EMClient;
import com.hyphenate.chat.EMConversation;
import com.hyphenate.chat.EMMessage;
import com.hyphenate.chat.EMTextMessageBody;
import com.hyphenate.exceptions.HyphenateException;
import com.nostra13.universalimageloader.core.ImageLoader;

import java.util.ArrayList;
import java.util.List;

import example.personal.connection.Constants;
import example.personal.connection.MyApplication;
import example.personal.connection.R;
import example.personal.connection.Utils.CommonUtils;
import example.personal.connection.Utils.DateUtils;
import example.personal.connection.module.emchat.EMChatManager;
import example.personal.connection.module.emchat.IMMessage;
import example.personal.connection.widget.PullLoadListView;

/**
 * 聊天界面
 * @author sunjichang
 * @version 1.0
 * @date 2017/3/2 00:36
 * @upate
 */
public class IMChatActivity extends BaseActivity implements View.OnClickListener,
        SwipeRefreshLayout.OnRefreshListener{
    private PullLoadListView lvContent;
    private List<IMMessage> mDataSet = null;
    private IMMessage message;
    private final long mIntervalTime = 5 * 60 * 1000;

    private Handler mHandler = new Handler(new Handler.Callback() {
        @Override
        public boolean handleMessage(Message msg) {
            if(msg.what == 1) {
                onSuccess((EMMessage) msg.obj,false);
            }else if(msg.what == 2){
                List<EMMessage> list = (List<EMMessage>) msg.obj;
                for (int i = 0 ; i < list.size(); i++){
                    onSuccess(list.get(i),true);
                }
            }
            return false;
        }
    });

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        setContentView(R.layout.layout_imchat);
        message = (IMMessage) getIntent().getParcelableExtra("content");
        mDataSet = new ArrayList<>();
        lvContent = (PullLoadListView) findViewById(R.id.lv_content);
        findViewById(R.id.send).setOnClickListener(this);
        lvContent.setAdapter(adapter);
        lvContent.setLoadingListener(new PullLoadListView.PullLoadListener() {
            @Override
            public void loading() {
                handlerRefresh();
            }
        });
        findViewById(R.id.title_bar_img_back).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        TextView txtTitle = (TextView) findViewById(R.id.title_bar_txt_title);
        txtTitle.setText(null != message ? message.receiveName : "");
        EMChatManager.getInstance().addMessageListener(new EMMessageListener() {
            @Override
            public void onMessageReceived(List<EMMessage> messages) {
                Message msg = mHandler.obtainMessage();
                msg.obj = messages;
                msg.what = 2;
                mHandler.sendMessage(msg);
            }

            @Override
            public void onCmdMessageReceived(List<EMMessage> messages) {

            }

            @Override
            public void onMessageRead(List<EMMessage> messages) {

            }

            @Override
            public void onMessageDelivered(List<EMMessage> messages) {

            }

            @Override
            public void onMessageChanged(EMMessage message, Object change) {

            }
        });
        super.onCreate(savedInstanceState);
    }

    @Override
    protected void onResume() {
        getData();
        super.onResume();
    }

    private void getData(){
        if(null == message || CommonUtils.isEmpty(message.receiveUserName)){
            return;
        }
        Log.e("TAG","message.receiveUserName="+message.receiveUserName);
        EMConversation conversation = EMClient.getInstance().chatManager().getConversation(message.receiveUserName,
                EMConversation.EMConversationType.Chat,false);
        Log.e("TAG","conversation=NULL="+(conversation == null));
        if(null == conversation){
            return;
        }
        conversation.markAllMessagesAsRead();
        EMChatManager.getInstance().flushConversation();
        List<EMMessage> list = conversation.getAllMessages();
        if(null == list){
            return;
        }
        mDataSet = new ArrayList<>();
        IMMessage msg = null;
        EMMessage emMessage = null;
        Log.e("TAG","list.size="+list.size());
        for (int i = 0; i < list.size();i++){
            msg = new IMMessage();
            emMessage = list.get(i);
            Log.e("TAG","emMessage="+emMessage.toString());
            try {
                msg.sendName = emMessage.getStringAttribute("sendName");
                msg.sendPhoto =emMessage.getStringAttribute("sendPhoto");
                msg.sendId = emMessage.getStringAttribute("sendId");
                msg.sendUserName = emMessage.getFrom();
                msg.receiveUserName = emMessage.getTo();
                msg.receivePhoto = emMessage.getStringAttribute("receivePhoto");
                msg.receiveId = emMessage.getStringAttribute("receiveId");
                msg.receiveName = emMessage.getStringAttribute("receiveName");
            } catch (HyphenateException e) {
                e.printStackTrace();
            }
            msg.message = list.get(i);
            Log.e("TAG","msg="+msg);
            mDataSet.add(msg);
        }
        adapter.notifyDataSetChanged();
    }

    private void handlerRefresh(){
        EMConversation conversation = EMClient.getInstance().chatManager().getConversation(message.receiveUserName,
                EMConversation.EMConversationType.Chat,false);
        if(null == mDataSet || mDataSet.size() < 1){
            lvContent.loadComplete();
            return;
        }
        IMMessage msg = mDataSet.get(0);
        String startMsgId = msg.message.getMsgId();
        int pagesize =  20;
        List<EMMessage> messages = conversation.loadMoreMsgFromDB(startMsgId, pagesize);
        if(null == messages){
            lvContent.loadComplete();
            return;
        }
        List<IMMessage> result = new ArrayList<>();
        for (int i = 0; i < messages.size(); i ++ ){
            msg = new IMMessage(messages.get(i),false);
            result.add(msg);
        }
        mDataSet.addAll(0,result);
        conversation.markAllMessagesAsRead();
        adapter.notifyDataSetChanged();
        lvContent.loadComplete();
    }

    @Override
    protected void onDestroy() {
        EMChatManager.getInstance().popMessageListener();
        EMConversation conversation = EMClient.getInstance().chatManager().getConversation(message.receiveUserName,
                EMConversation.EMConversationType.Chat,false);
        if(null != conversation){
            conversation.markAllMessagesAsRead();
        }
        super.onDestroy();
    }

    @Override
    public void onClick(View v) {
        EditText editText = (EditText) findViewById(R.id.etxt_content);
        if(editText.length() <= 0){
            return;
        }
        String content = editText.getText().toString();
        String receiver = message.receiveUserName;
        String sendId = message.sendId;
        String sendName = message.sendName;
        String sendPhoto = message.sendPhoto;
        String receiveName = message.receiveName;
        String receivePhoto = message.receivePhoto;
        String receiveId = message.receiveId;
        EMChatManager.getInstance().sendMsg(content, receiver, sendName, sendPhoto,
                sendId,receiveName,receivePhoto,receiveId,  EMChatManager.CHATTYPE_FRIEND, new EMChatManager.IMCallback() {
                    @Override
                    public void onSuccess(EMMessage m) {
                        //onSuccess(m);
                        Message msg = mHandler.obtainMessage();
                        msg.obj = m;
                        msg.what = 1;
                        mHandler.sendMessage(msg);
                    }

                    @Override
                    public void onError(int code, String error) {

                    }

                    @Override
                    public void onProgress(int progress, String status) {

                    }
                });
    }

    private void onSuccess(EMMessage m,boolean isReceiver){
        IMMessage msg = new IMMessage();
        try {
            msg.sendName = m.getStringAttribute("sendName");
            msg.sendPhoto =m.getStringAttribute("sendPhoto");
            msg.sendId = m.getStringAttribute("sendId");
            msg.sendUserName = m.getFrom();
            msg.receiveUserName = m.getTo();
            msg.receivePhoto = m.getStringAttribute("receivePhoto");
            msg.receiveId = m.getStringAttribute("receiveId");
            msg.receiveName = m.getStringAttribute("receiveName");
        } catch (HyphenateException e) {
            e.printStackTrace();
        }
        if(isReceiver && !msg.sendUserName.equals(message.receiveUserName)){
            return;
        }
        msg.message = m;
        Log.e("TAG","msg="+msg);
        mDataSet.add(msg);
        adapter.notifyDataSetChanged();
        lvContent.setSelection(mDataSet.size()-1);
        ((EditText) findViewById(R.id.etxt_content)).setText(null);
    }


    private BaseAdapter adapter = new BaseAdapter(){

        @Override
        public int getCount() {
            return mDataSet.size();
        }

        @Override
        public Object getItem(int position) {
            return mDataSet.get(position);
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(int position, android.view.View convertView, ViewGroup parent) {
            ViewHolder holder = null;
            IMMessage msg = mDataSet.get(position);
            boolean isSelf = msg.sendId.equals(MyApplication.getInstance().getUserId());
            Log.e("TAG","isSelf="+isSelf+",msg.message="+((EMTextMessageBody)msg.message.getBody()).getMessage());
            int type = getItemViewType(position);
            if(null == convertView){
                holder = new ViewHolder();
                if(type == 0){
                    convertView = getLayoutInflater().inflate(R.layout.layout_imchat_item_user,null);
                    holder.dateUser = (TextView)convertView.findViewById(R.id.txt_time_user);
                    holder.photoUser = (ImageView)convertView.findViewById(R.id.img_head_user);
                    holder.contentUser = (TextView)convertView.findViewById(R.id.txt_content_user);
                }else{
                    convertView = getLayoutInflater().inflate(R.layout.layout_imchat_item_friend,null);
                    holder.photoFriend = (ImageView)convertView.findViewById(R.id.img_head_friend);
                    holder.name = (TextView)convertView.findViewById(R.id.txt_name);
                    holder.contentFriend = (TextView)convertView.findViewById(R.id.txt_content_friend);
                    holder.dateFriend = (TextView)convertView.findViewById(R.id.txt_time_friend);
                }
                convertView.setTag(holder);
            }else{
                holder = (ViewHolder) convertView.getTag();
            }
            long time = msg.message.getMsgTime();
            long preTime = position < 1 ? 0 : mDataSet.get(position - 1).message.getMsgTime();
            boolean show = isShow(time,preTime);
            if(type == 0){
                ImageLoader.getInstance().displayImage(Constants.formatImageUrl(msg.sendPhoto),holder.photoUser,
                        CommonUtils.getUserIconOptions());
                holder.contentUser.setText(((EMTextMessageBody)msg.message.getBody()).getMessage());
                if(show){
                    holder.dateUser.setText(DateUtils.getChatTime(time));
                    holder.dateUser.setVisibility(View.VISIBLE);
                }else{
                    holder.dateUser.setVisibility(View.INVISIBLE);
                }
            }else{
                ImageLoader.getInstance().displayImage(Constants.formatImageUrl(msg.sendPhoto),holder.photoFriend,
                        CommonUtils.getUserIconOptions());
                holder.name.setText(msg.sendName);
                holder.contentFriend.setText(((EMTextMessageBody)msg.message.getBody()).getMessage());
                if(show){
                    holder.dateFriend.setText(DateUtils.getChatTime(time));
                    holder.dateFriend.setVisibility(View.VISIBLE);
                }else{
                    holder.dateFriend.setVisibility(View.INVISIBLE);
                }
            }
            return convertView;
        }

        @Override
        public int getViewTypeCount() {
            return 2;
        }

        @Override
        public int getItemViewType(int position) {
            IMMessage msg = mDataSet.get(position);
            boolean isSelf = msg.sendId.equals(MyApplication.getInstance().getUserId());
            return isSelf ? 0 : 1;
        }

        private boolean isShow(long currTime,long preTime){
            return currTime - preTime > mIntervalTime;
        }
    };

    @Override
    public void onRefresh() {
        handlerRefresh();
    }

    private class ViewHolder{
        ImageView photoFriend;
        TextView name;
        TextView contentFriend;
        TextView dateFriend;
        TextView dateUser;
        ImageView photoUser;
        TextView contentUser;
    }
}
