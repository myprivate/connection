package example.personal.connection.activity;

import android.app.Activity;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.View;
import android.widget.ImageView;
import android.widget.SearchView;
import android.widget.TextView;

import com.android.volley.Response;
import com.android.volley.VolleyError;

import java.util.ArrayList;
import java.util.List;

import example.personal.connection.Constants;
import example.personal.connection.MyApplication;
import example.personal.connection.R;
import example.personal.connection.Utils.ToastUtils;
import example.personal.connection.adapter.SearchResultAdapter;
import example.personal.connection.entity.BaseBean;
import example.personal.connection.entity.LoginInfoBean;
import example.personal.connection.entity.SearchResultBean;
import example.personal.connection.manager.ServiceManager;
import example.personal.connection.widget.XListView;

/**
 * Created by wuqian on 2017/2/23.
 * mail: wuqian@ilingtong.com
 * Description:搜索页面
 */
public class SearchActivity extends Activity implements View.OnClickListener, XListView.IXListViewListener {
    private ImageView titleBarImgBack;
    private TextView titleBarTxtTitle;
    private TextView titleBarTxtAction;
    private SearchView alumniSvSearch;
    private TextView searchTxtNouser;
    private XListView searchListView;
    private String keyword;
    private int currentPage;
    private List<LoginInfoBean> list = new ArrayList<>();
    private SearchResultAdapter adapter;
    private int addPosition;   //表示当前添加的是列表第几位好友

    private int mState = 0;
    public static final int WHAT_NODATA = 0;
    public static final int WHAT_REFRESH = 1;
    public static final int WHAT_ADD = 2;

    Handler handler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case WHAT_NODATA:
                    searchTxtNouser.setVisibility(View.VISIBLE);
                    break;
                case WHAT_REFRESH:
                    searchTxtNouser.setVisibility(View.GONE);
                    searchListView.setRefreshTime();
                    adapter.notifyDataSetChanged();
                    break;
                case WHAT_ADD:  //添加好友
                    addPosition = msg.arg1;
                    addFriend(list.get(addPosition).id);
                    break;
            }
        }
    };


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search_layout);
        initView();
    }

    /**
     * 初始化view
     */
    private void initView() {
        titleBarImgBack = (ImageView) findViewById(R.id.title_bar_img_back);
        titleBarImgBack.setOnClickListener(this);
        titleBarTxtTitle = (TextView) findViewById(R.id.title_bar_txt_title);
        titleBarTxtTitle.setText(getString(R.string.search));
        titleBarTxtAction = (TextView) findViewById(R.id.title_bar_txt_action);
        titleBarTxtAction.setVisibility(View.GONE);
        alumniSvSearch = (SearchView) findViewById(R.id.alumni_sv_search);
        searchTxtNouser = (TextView) findViewById(R.id.search_txt_nouser);
        searchListView = (XListView) findViewById(R.id.search_list_users);
        searchListView.setPullLoadEnable(false);
        searchListView.setXListViewListener(this,0);
        adapter = new SearchResultAdapter(list, handler, this);
        searchListView.setAdapter(adapter);
        alumniSvSearch.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String s) {
                keyword = s;
                mState = 0;
                list.clear();
                searchUserByKeyword(keyword);
                return true;
            }

            @Override
            public boolean onQueryTextChange(String s) {
                return true;
            }
        });
    }


    /**
     * 添加好友
     *
     * @param friendId
     */
    private void addFriend(String friendId) {
        ServiceManager.friendApply(MyApplication.getInstance().getUserId(), friendId, "", addListener(), errorListener);
    }

    private Response.Listener<BaseBean> addListener() {
        return new Response.Listener<BaseBean>() {
            @Override
            public void onResponse(BaseBean response) {
                if (Constants.SUCCESSCODE.equals(response.code)) {
                    list.remove(addPosition);
                    handler.sendEmptyMessage(WHAT_REFRESH);
                } else {
                    ToastUtils.showLongToast(SearchActivity.this, response.message);
                }
            }
        };
    }

    /**
     * 调用接口搜索校友
     *
     * @param keyword
     */
    private void searchUserByKeyword(String keyword) {
        ServiceManager.searchByKeyword(MyApplication.getInstance().getUserId(), keyword, Constants.PAGE_SIZE, currentPage, listener(), errorListener);
    }

    private Response.Listener<SearchResultBean> listener() {

        return new Response.Listener<SearchResultBean>() {
            @Override
            public void onResponse(SearchResultBean response) {
                if (Constants.SUCCESSCODE.equals(response.code)) {
                    if (response.data.data == null || response.data.data.size() < 1) {
                        handler.sendEmptyMessage(0);
                    } else {
                        if(mState == 0){
                            list.clear();
                        }
                        list.addAll(response.data.data);
                        currentPage = response.data.pageNum;
                        searchListView.setPullLoadEnable(!response.data.lastPage);
                        handler.sendEmptyMessage(1);
                    }
                }
            }
        };
    }

    private Response.ErrorListener errorListener = new Response.ErrorListener() {
        @Override
        public void onErrorResponse(VolleyError error) {
            ToastUtils.showLongToast(SearchActivity.this, error.getMessage() + "");
        }
    };

    @Override
    public void onClick(View view) {

        switch (view.getId()) {
            case R.id.title_bar_img_back:
                finish();
                break;
        }
    }

    @Override
    public void onRefresh(int id) {
        list.clear();
        mState = 0;
        searchUserByKeyword(keyword);
    }

    @Override
    public void onLoadMore(int id) {
        mState = 1;
        searchUserByKeyword(keyword);
    }
}
