package example.personal.connection.activity;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.Message;
import android.provider.MediaStore;
import android.text.TextUtils;
import android.view.Gravity;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.NumberPicker;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.google.gson.Gson;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.mime.HttpMultipartMode;
import org.apache.http.entity.mime.MultipartEntityBuilder;
import org.apache.http.entity.mime.content.ByteArrayBody;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.CharsetUtils;
import org.apache.http.util.EntityUtils;

import java.io.File;
import java.io.InputStream;
import java.util.Calendar;

import example.personal.connection.Constants;
import example.personal.connection.R;
import example.personal.connection.Utils.CommonUtils;
import example.personal.connection.Utils.DialogUtils;
import example.personal.connection.Utils.ToastUtils;
import example.personal.connection.entity.ImageLoadResult;
import example.personal.connection.entity.RegisterResultBean;
import example.personal.connection.manager.ServiceManager;

/**
 * Created by wuqian on 2017/2/21.
 * mail: wuqian@ilingtong.com
 * Description:注册
 */
public class RegisterActivity extends Activity implements View.OnClickListener {
    private ImageView titleBarImgBack;
    private TextView titleBarTxtTitle;
    private TextView titleBarTxtAction;
    private EditText regsterEditName;
    private ImageView registerImgHeadIcon;
    private EditText registerEditBirthday;
    private EditText registerEditEmail;
    private EditText registerEditCity;
    private EditText registerEditUniversity;
    private EditText registerEditYearOfGraduation;
    private EditText registerEditMajor;
    private EditText registerEditClassNumber;
    private EditText registerEditPhone;
    private EditText registerEditPassword;
    private EditText registerEditRepeatPassword;
    private Button registerBtnRegister;

    private String name;
    private String headIcon;
    private String birthday;
    private String email;
    private String city;
    private String university;
    private String yearOfGraduation;
    private String major;
    private String classNumber;
    private String phone;
    private String password;
    private String repeatPassword;

    private final int REQUESTCODE_CHOOSE_PIC = 1002;
    private final int PHOTO_REQUEST_CAMERA = 1003;
    private static final String PHOTO_FILE_NAME = "temp_photo.jpg";
    private final int REQUEST_CROP_IMG = 1004;
    private File tempFile;
    private Bitmap headBitmap;
    private Dialog dialog;
    private String imagePath;
    private boolean flag = true;  //为true表示可以点击上传头像，否则不可上传头像

    Handler handler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case 0:
                    Gson gson = new Gson();
                    ImageLoadResult imageLoadResult = gson.fromJson((String) msg.obj, ImageLoadResult.class);
                    if (Constants.SUCCESSCODE.equals(imageLoadResult.code)) {
                        imagePath = imageLoadResult.data;
                        registerImgHeadIcon.setImageBitmap(headBitmap);
                    } else {
                        ToastUtils.showLongToast(RegisterActivity.this, imageLoadResult.message);
                    }
                    break;
                case 1:
                    Toast.makeText(getApplicationContext(), "服务器返回异常", Toast.LENGTH_LONG).show();
                    break;
            }
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register_layout);
        initView();

    }

    /**
     * 初始化view
     */
    private void initView() {
        dialog = DialogUtils.createLoadingDialog(this);
        titleBarImgBack = (ImageView) findViewById(R.id.title_bar_img_back);
        titleBarTxtTitle = (TextView) findViewById(R.id.title_bar_txt_title);
        titleBarTxtTitle.setText(getString(R.string.register));
        titleBarTxtAction = (TextView) findViewById(R.id.title_bar_txt_action);
        titleBarTxtAction.setVisibility(View.GONE);
        regsterEditName = (EditText) findViewById(R.id.regster_edit_name);
        registerImgHeadIcon = (ImageView) findViewById(R.id.regoster_img_head_icon);
        registerEditBirthday = (EditText) findViewById(R.id.register_edit_birthday);
        registerEditBirthday.setOnClickListener(this);
        registerEditEmail = (EditText) findViewById(R.id.register_edit_email);
        registerEditCity = (EditText) findViewById(R.id.register_edit_city);
        registerEditUniversity = (EditText) findViewById(R.id.register_edit_university);
        registerEditYearOfGraduation = (EditText) findViewById(R.id.register_edit_year_of_graduation);
        registerEditYearOfGraduation.setOnClickListener(this);
        registerEditMajor = (EditText) findViewById(R.id.register_edit_major);
        registerEditClassNumber = (EditText) findViewById(R.id.register_edit_class_number);
        registerEditPhone = (EditText) findViewById(R.id.register_edit_phone);
        registerEditPassword = (EditText) findViewById(R.id.register_edit_password);
        registerEditRepeatPassword = (EditText) findViewById(R.id.register_edit_repeat_password);
        registerBtnRegister = (Button) findViewById(R.id.register_btn_register);

        registerBtnRegister.setOnClickListener(this);
        titleBarImgBack.setOnClickListener(this);
        registerImgHeadIcon.setOnClickListener(this);
    }

    /**
     * 检测输入内容是否完整
     */
    private boolean checkParam() {
        name = regsterEditName.getText().toString();
        headIcon = "";
        birthday = registerEditBirthday.getText().toString();
        email = registerEditEmail.getText().toString();
        city = registerEditCity.getText().toString();
        university = registerEditUniversity.getText().toString();
        yearOfGraduation = registerEditYearOfGraduation.getText().toString();
        major = registerEditMajor.getText().toString();
        classNumber = registerEditClassNumber.getText().toString();
        phone = registerEditPhone.getText().toString();
        password = registerEditPassword.getText().toString();
        repeatPassword = registerEditRepeatPassword.getText().toString();

        if (TextUtils.isEmpty(name)) {
            ToastUtils.showLongToast(this, "input name please");
            return false;
        } else if (TextUtils.isEmpty(birthday)) {
            ToastUtils.showLongToast(this, "input birthday please");
            return false;
        } else if (TextUtils.isEmpty(email)) {
            ToastUtils.showLongToast(this, "input email please");
            return false;
        } else if (TextUtils.isEmpty(city)) {
            ToastUtils.showLongToast(this, "input city please");
            return false;
        } else if (TextUtils.isEmpty(university)) {
            ToastUtils.showLongToast(this, "input university please");
            return false;
        } else if (TextUtils.isEmpty(yearOfGraduation)) {
            ToastUtils.showLongToast(this, "input yaerOfGraduation please");
            return false;
        } else if (TextUtils.isEmpty(major)) {
            ToastUtils.showLongToast(this, "input major please");
            return false;
        } else if (TextUtils.isEmpty(classNumber)) {
            ToastUtils.showLongToast(this, "input classNumber please");
            return false;
        } else if (TextUtils.isEmpty(phone)) {
            ToastUtils.showLongToast(this, "input phone please");
            return false;
        } else if (TextUtils.isEmpty(password)) {
            ToastUtils.showLongToast(this, "input password please");
            return false;
        } else if (TextUtils.isEmpty(repeatPassword)) {
            ToastUtils.showLongToast(this, "input repeatPassword please");
            return false;
        } else if (!password.equals(repeatPassword)) {
            ToastUtils.showLongToast(this, "password not atypism");
            return false;
        } else {
            return true;
        }

    }

    /**
     * 注册
     */
    private void doRegister() {
        if (checkParam()) {
            dialog.show();
            ServiceManager.doRegister(name, birthday, email, city, university, yearOfGraduation, major, classNumber, phone, password, imagePath, listener(), errorListener());
        }
    }

    private Response.Listener<RegisterResultBean> listener() {
        return new Response.Listener<RegisterResultBean>() {
            @Override
            public void onResponse(RegisterResultBean response) {
                dialog.dismiss();
                if (Constants.SUCCESSCODE.equals(response.code)) {
                    ToastUtils.showLongToast(RegisterActivity.this, "success");
                    RegisterActivity.this.finish();
                }else {
                    ToastUtils.showLongToast(RegisterActivity.this,response.message);
                }
            }
        };
    }

    private Response.ErrorListener errorListener() {
        return new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                dialog.dismiss();
                ToastUtils.showLongToast(RegisterActivity.this, getString(R.string.syserro));
            }
        };
    }

    /**
     * 弹出日历选择器
     */
    private void showDatePickerDialog() {
        Calendar calendar = Calendar.getInstance();
        DatePickerDialog datePickerDialog = new DatePickerDialog(RegisterActivity.this, new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker datePicker, int year, int month, int dayofmonth) {
                String date = year + "/" + (month + 1) + "/" + dayofmonth;
                registerEditBirthday.setText(date);
            }
        }
                , calendar.get(Calendar.YEAR)
                , calendar.get(Calendar.MONTH)
                , calendar.get(Calendar.DAY_OF_MONTH));

        Calendar maxCalendar = Calendar.getInstance();
        datePickerDialog.getDatePicker().setMaxDate(maxCalendar.getTimeInMillis());
        datePickerDialog.show();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == RESULT_OK) {
            switch (requestCode) {
//                case 1000:
//                    registerEditYearOfGraduation.setText(data.getExtras().get("year") + "");
//                    break;
                case 1001:  //拍照or选择照片
                    setHeadIcon(data.getIntExtra("type", 0));
                    break;
                case REQUESTCODE_CHOOSE_PIC:  //选择照片后处理
                    if (data != null) {
                        // 得到图片的全路径
                        Uri uri = data.getData();
                        //CropImageUtils.crop(RegisterActivity.this, uri, REQUEST_CROP_IMG, imageUri);
                        crop(uri);
                    }
                    break;
                case PHOTO_REQUEST_CAMERA:  //拍照后处理
                    if (hasSdcard()) {
                        tempFile = new File(Environment.getExternalStorageDirectory(),
                                PHOTO_FILE_NAME);
                        crop(Uri.fromFile(tempFile));
                    } else {
                        ToastUtils.showLongToast(RegisterActivity.this, "no sdcard!");
                    }
                    break;
                case REQUEST_CROP_IMG:  //裁剪后处理
                    try {
                        headBitmap = data.getParcelableExtra("data");
                        //registerImgHeadIcon.setImageBitmap(headBitmap);
                        new Thread() {
                            @Override
                            public void run() {
                                flag = false;
                                CommonUtils.uploadImage(Constants.IMAGE_UPLOAD, headBitmap,handler);
                                flag = true;
                            }
                        }.start();

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    break;
            }
        }
    }

    @Override
    public void onClick(View view) {

        switch (view.getId()) {
            case R.id.register_btn_register:
                doRegister();
                break;
            case R.id.title_bar_img_back:
                finish();
                break;
            case R.id.register_edit_birthday:   //生日
                showDatePickerDialog();
                break;
            case R.id.register_edit_year_of_graduation:   //毕业年份
                //GraduationPickerDialog.launch(RegisterActivity.this, 1000);
                createAlertNumberPicker();
                break;
            case R.id.regoster_img_head_icon:  //设置头像
                if (flag) {
                    GetPhotoSelectDialogActivity.launcher(this, 1001);
                }
                break;
        }
    }

    /**
     * 显示毕业年份选择器
     */
    private void createAlertNumberPicker() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        final NumberPicker numberPicker = new NumberPicker(this);
        Calendar calendar = Calendar.getInstance();
        numberPicker.setMaxValue(calendar.get(Calendar.YEAR));
        numberPicker.setMinValue(calendar.get(Calendar.YEAR) - 100);
        numberPicker.setValue(calendar.get(Calendar.YEAR));
        yearOfGraduation = calendar.get(Calendar.YEAR) + "";
        numberPicker.setOnValueChangedListener(new NumberPicker.OnValueChangeListener() {
            @Override
            public void onValueChange(NumberPicker numberPicker, int oldNum, int newNum) {
                yearOfGraduation = newNum + "";
            }
        });
        final FrameLayout parent = new FrameLayout(this);
        parent.addView(numberPicker, new FrameLayout.LayoutParams(
                FrameLayout.LayoutParams.WRAP_CONTENT,
                FrameLayout.LayoutParams.WRAP_CONTENT,
                Gravity.CENTER));
        builder.setView(parent);
        builder.setTitle(getString(R.string.year_of_graduation))
                .setCancelable(false)
                .setPositiveButton(getString(R.string.ok), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        registerEditYearOfGraduation.setText(yearOfGraduation);
                    }
                });
        AlertDialog alertDialog = builder.create();
        alertDialog.show();
    }

    /**
     * 选择获取图片方式后处理
     *
     * @param type
     */
    private void setHeadIcon(int type) {

        if (GetPhotoSelectDialogActivity.ACTION_SELECT_PIC == type) {
            Intent intent = new Intent(Intent.ACTION_PICK);
            intent.setType("image/*");
            startActivityForResult(intent, REQUESTCODE_CHOOSE_PIC);
        } else if (GetPhotoSelectDialogActivity.ACTION_TAKE_PIC == type) {
            camera();
        }
    }

    /**
     * 启动相机拍照
     */
    public void camera() {
        Intent intent = new Intent("android.media.action.IMAGE_CAPTURE");
        // 判断存储卡是否可以用，可用进行存储
        if (hasSdcard()) {
            intent.putExtra(MediaStore.EXTRA_OUTPUT,
                    Uri.fromFile(new File(Environment
                            .getExternalStorageDirectory(), PHOTO_FILE_NAME)));
        }
        startActivityForResult(intent, PHOTO_REQUEST_CAMERA);
    }

    private boolean hasSdcard() {
        if (Environment.getExternalStorageState().equals(
                Environment.MEDIA_MOUNTED)) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * 剪切头像图片
     *
     * @param uri
     * @function:
     * @author:Jerry
     * @date:2013-12-30
     */
    private void crop(Uri uri) {
        // 裁剪图片意图
        Intent intent = new Intent("com.android.camera.action.CROP");
        intent.setDataAndType(uri, "image/*");
        intent.putExtra("crop", "true");
        // 裁剪框的比例，1：1
        intent.putExtra("aspectX", 1);
        intent.putExtra("aspectY", 1);
        // 裁剪后输出图片的尺寸大小
        intent.putExtra("outputX", 280);
        intent.putExtra("outputY", 280);
        // 图片格式
        intent.putExtra("outputFormat", "JPEG");
        intent.putExtra("noFaceDetection", true);// 取消人脸识别
        intent.putExtra("return-data", true);// true:不返回uri，false：返回uri
        startActivityForResult(intent, REQUEST_CROP_IMG);
    }

    /**
     * 识别护照照片
     *
     * @param url
     * @return
     */
//    public final void doPost(String url, Bitmap bitmap) {
//        String result = "";
//        try {
//            HttpClient client = new DefaultHttpClient(); // 1.创建httpclient对象
//            HttpPost post = new HttpPost(url); // 2.通过url创建post方法
//            post.setHeader("accept", "application/json");
//
//            // ***************************************<向post方法中封装实体>************************************//3.向post方法中封装实体
//            MultipartEntityBuilder builder = MultipartEntityBuilder.create(); // 实例化实体构造器
//            builder.setMode(HttpMultipartMode.BROWSER_COMPATIBLE); // 设置浏览器兼容模式
//            ByteArrayBody bab = new ByteArrayBody(CommonUtils.Bitmap2Bytes(bitmap), "pic.png");
//            builder.addPart("file", bab); // 添加"file"字段及其值
//
//            HttpEntity reqEntity = builder.setCharset(CharsetUtils.get("UTF-8")).build(); // 设置请求的编码格式，并构造实体
//
//            post.setEntity(reqEntity);
//            // **************************************</向post方法中封装实体>************************************
//
//            HttpResponse response = client.execute(post); // 4.执行post方法，返回HttpResponse的对象
//            if (response.getStatusLine().getStatusCode() == 200) { // 5.如果返回结果状态码为200，则读取响应实体response对象的实体内容，并封装成String对象返回
//                result = EntityUtils.toString(response.getEntity(), "UTF-8");
//                Message mes = new Message();
//                mes.obj = result;
//                mes.what = 0;
//                handler.sendMessage(mes);
//
//            } else {
//                Message mes = new Message();
//                mes.obj = "服务器返回异常";
//                mes.what = 1;
//                handler.sendMessage(mes);
//            }
//            try {
//                HttpEntity e = response.getEntity(); // 6.关闭资源
//                if (e != null) {
//                    InputStream instream = e.getContent();
//                    instream.close();
//                }
//            } catch (Exception e) {
//                e.printStackTrace();
//            } finally {
//                ((InputStream) response).close();
//            }
//        } catch (Exception e) {
//            e.printStackTrace();
//        } finally {
//        }
//    }
}
