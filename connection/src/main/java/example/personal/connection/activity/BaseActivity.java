package example.personal.connection.activity;

import android.app.Activity;
import android.os.Bundle;

/**
 * Created by wuqian on 2017/2/21.
 * mail: wuqian@ilingtong.com
 * Description:
 */
public class BaseActivity extends Activity{
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }
}
