package example.personal.connection.activity;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.widget.RadioGroup;

import example.personal.connection.R;

/**
 * Created by wuqian on 2017/2/22.
 * mail: wuqian@ilingtong.com
 * Description:主界面
 */
public class MainFragmentActivity extends FragmentActivity {

    private RadioGroup radioGroup;
    private Fragment[] fragments;
    private FragmentManager manager;
    private FragmentTransaction transaction;
    public static MainFragmentActivity instance;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        instance = this;
        setContentView(R.layout.activity_main_layout);

        radioGroup = (RadioGroup) findViewById(R.id.main_host_rg);
        fragments = new Fragment[4];
        manager = getSupportFragmentManager();
        fragments[0] = manager.findFragmentById(R.id.main_fragment_message);
        fragments[1] = manager.findFragmentById(R.id.main_fragment_alumni);
        fragments[2] = manager.findFragmentById(R.id.main_fragment_moments);
        fragments[3] = manager.findFragmentById(R.id.main_fragment_me);

        transaction = manager.beginTransaction();
        transaction.hide(fragments[0]).hide(fragments[1]).hide(fragments[2]).hide(fragments[3]);
        transaction.show(fragments[0]);
        transaction.commit();
        setFragmentIndicator();
    }

    /**
     * 切换界面
     */
    private void setFragmentIndicator() {

        radioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {

            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                transaction = manager.beginTransaction()
                        .hide(fragments[0])
                        .hide(fragments[1])
                        .hide(fragments[2])
                        .hide(fragments[3]);
                if (checkedId == R.id.main_rbt_host_message) {
                    transaction.show(fragments[0]).commit();

                } else if (checkedId == R.id.main_rbt_host_alumni) {
                    transaction.show(fragments[1]).commit();

                } else if (checkedId == R.id.main_rbt_host_monments) {
                    transaction.show(fragments[2]).commit();

                } else if (checkedId == R.id.main_rbt_host_me) {
                    transaction.show(fragments[3]).commit();
                }
            }
        });
    }

}
