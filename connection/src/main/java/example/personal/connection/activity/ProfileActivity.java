package example.personal.connection.activity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.nostra13.universalimageloader.core.ImageLoader;

import example.personal.connection.Constants;
import example.personal.connection.MyApplication;
import example.personal.connection.R;
import example.personal.connection.Utils.CommonUtils;
import example.personal.connection.Utils.ToastUtils;
import example.personal.connection.entity.BaseBean;
import example.personal.connection.entity.LoginInfoBean;
import example.personal.connection.manager.ServiceManager;
import example.personal.connection.module.emchat.IMMessage;

/**
 * Created by wuqian on 2017/2/23.
 * mail: wuqian@ilingtong.com
 * Description:个人简介
 */
public class ProfileActivity extends Activity implements View.OnClickListener {
    private ImageView titleBarImgBack;
    private TextView titleBarTxtTitle;
    private ImageView profileImgHead;
    private TextView profileTxtName;
    private TextView profileTxtId;
    private TextView profileTxtName1;
    private TextView profileTxtPhone;
    private TextView profileTxtBirthday;
    private TextView profileTxtEmail;
    private TextView profileTxtCity;
    private TextView profileTxtUniversity;
    private TextView profileTxtYearOfGraduation;
    private TextView profileTxtMajor;
    private TextView profileTxtClassNumber;
    private Button profileBtnSendmsg;
    private Button profileBtnPosts;
    private Button btnDelete;

    public LoginInfoBean userInfo;


    /**
     * 以startActivityForResult的方式启动该页面
     * @param fragment
     * @param userinfo
     * @param requestCode
     */
    public static void launch(Fragment fragment, LoginInfoBean userinfo, int requestCode){
        Intent intent = new Intent(fragment.getActivity(),ProfileActivity.class);
        intent.putExtra("userinfo",userinfo);
        fragment.startActivityForResult(intent,requestCode);
    }
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile_layout);
        userInfo = (LoginInfoBean) getIntent().getSerializableExtra("userinfo");
        initView();
    }

    /**
     * 初始化viwe
     */
    private void initView() {
        titleBarImgBack = (ImageView) findViewById(R.id.title_bar_img_back);
        titleBarImgBack.setOnClickListener(this);
        titleBarTxtTitle = (TextView) findViewById(R.id.title_bar_txt_title);
        titleBarTxtTitle.setText(userInfo.name);

        profileImgHead = (ImageView) findViewById(R.id.profile_img_head);
        profileTxtName = (TextView) findViewById(R.id.profile_txt_name);
        profileTxtId = (TextView) findViewById(R.id.profile_txt_id);
        profileTxtName1 = (TextView) findViewById(R.id.profile_txt_name1);
        profileTxtPhone = (TextView) findViewById(R.id.profile_txt_phone);
        profileTxtBirthday = (TextView) findViewById(R.id.profile_txt_birthday);
        profileTxtEmail = (TextView) findViewById(R.id.profile_txt_email);
        profileTxtCity = (TextView) findViewById(R.id.profile_txt_city);
        profileTxtUniversity = (TextView) findViewById(R.id.profile_txt_university);
        profileTxtYearOfGraduation = (TextView) findViewById(R.id.profile_txt_year_of_graduation);
        profileTxtMajor = (TextView) findViewById(R.id.profile_txt_major);
        profileTxtClassNumber = (TextView) findViewById(R.id.profile_txt_class_number);
        profileBtnSendmsg = (Button) findViewById(R.id.profile_btn_sendmsg);
        profileBtnPosts = (Button) findViewById(R.id.profile_btn_posts);
        btnDelete = (Button) findViewById(R.id.profile_btn_delete);
        btnDelete.setOnClickListener(this);
        profileBtnSendmsg.setOnClickListener(this);
        profileBtnPosts.setOnClickListener(this);
        if(null == userInfo){
            return;
        }
        if (!TextUtils.isEmpty(userInfo.photo)){
            ImageLoader.getInstance().displayImage(Constants.formatImageUrl(userInfo.photo),profileImgHead, CommonUtils.getUserIconOptions());
        }
        profileTxtName.setText(userInfo.name+"");
        profileTxtId.setText(userInfo.phone+"");
        profileTxtName1.setText(userInfo.name+"");
        profileTxtPhone.setText(userInfo.phone+"");
        profileTxtBirthday.setText(userInfo.birthday+"");
        profileTxtEmail.setText(userInfo.email+"");
        profileTxtCity.setText(userInfo.city+"");
        profileTxtUniversity.setText(userInfo.university+"");
        profileTxtYearOfGraduation.setText(userInfo.yearOfGraduation+"");
        profileTxtMajor.setText(userInfo.major+"");
        profileTxtClassNumber.setText(userInfo.classNumber+"");
    }

    @Override
    public void onClick(View view) {

        switch (view.getId()) {
            case R.id.title_bar_img_back:
                finish();
                break;
            case R.id.profile_btn_sendmsg:  //发消息
                gotoMessage();
                break;
            case R.id.profile_btn_posts:   //朋友圈
                MyPostListActivity.launch(this,userInfo);
                break;
            case R.id.profile_btn_delete:   //删除好友
                ServiceManager.deleteFriend(userInfo.id, listener(), new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError volleyError) {
                        ToastUtils.showLongToast(ProfileActivity.this,getString(R.string.syserro));
                    }
                });
                break;
        }
    }

    /**
     * 删除好友接口调用成功的回调
     */
    private Response.Listener<BaseBean> listener(){
        return new Response.Listener<BaseBean>() {
            @Override
            public void onResponse(BaseBean baseBean) {
                if (Constants.SUCCESSCODE.equals(baseBean.code)){
                    setResult(RESULT_OK);
                    ProfileActivity.this.finish();
                }else {
                    ToastUtils.showLongToast(ProfileActivity.this,baseBean.message);
                }
            }
        };
    }

    /**
     * send msg
     */
    private void gotoMessage(){
        Intent intent = new Intent(this,IMChatActivity.class);
        IMMessage msg = new IMMessage();
        LoginInfoBean info = MyApplication.getInstance().getUserInfo();
        if(null != info) {
            msg.sendId = info.id;
            msg.sendName = info.name;
            msg.sendPhoto =info.photo;
            msg.sendUserName = info.voipUsername;
        }
        msg.receiveId = userInfo.id;
        msg.receiveName = userInfo.name;
        msg.receivePhoto = userInfo.photo;
        msg.receiveUserName = userInfo.voipUsername;
        intent.putExtra("content",msg);
        addConversation();
        startActivity(intent);
    }

    private void addConversation(){
        ServiceManager.addConversation(MyApplication.getInstance().getUserId(), userInfo.id, new Response.Listener<BaseBean>() {
            @Override
            public void onResponse(BaseBean response) {

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        });
    }
}
