package example.personal.connection.activity;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.WindowManager;
import android.widget.Toast;

import com.android.volley.Response;
import com.android.volley.VolleyError;

import example.personal.connection.Constants;
import example.personal.connection.MyApplication;
import example.personal.connection.R;
import example.personal.connection.entity.LoginResultBean;
import example.personal.connection.manager.ServiceManager;
import example.personal.connection.module.emchat.EMChatManager;

/**
 * Created by wuqian on 2017/3/4.
 * mail: wuqian@ilingtong.com
 * Description:
 */
public class WelcomeActivity extends Activity {
    private SharedPreferences sp;
    private String username;
    private String password;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);//全屏显示
        setContentView(R.layout.activity_welcome_layout);
        sp = getSharedPreferences("login", Context.MODE_PRIVATE);
        username = sp.getString("username","");
        password = sp.getString("password","");
        if (TextUtils.isEmpty(username) || TextUtils.isEmpty(password)){
            toLogin();
        }else {
            doLogin();
        }
    }

    /**
     * 登录
     */
    private void doLogin() {

        ServiceManager.doLogin(username, password, new Response.Listener<LoginResultBean>() {
            @Override
            public void onResponse(LoginResultBean response) {

                if (Constants.SUCCESSCODE.equals(response.code)) {
                    MyApplication.getInstance().setUserInfo(response.data);
                    String voipUsername = response.data.voipUsername;
                    String voipPwd = response.data.voipPwd;
                    if(!TextUtils.isEmpty(voipUsername)) {
                        EMChatManager.getInstance().loginEM(voipUsername,voipPwd);
                    }
                    Intent intent = new Intent(WelcomeActivity.this, MainFragmentActivity.class);
                    startActivity(intent);
                    WelcomeActivity.this.finish();
                } else {
                    Toast.makeText(WelcomeActivity.this, response.message, Toast.LENGTH_LONG).show();
                    toLogin();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(WelcomeActivity.this, getString(R.string.syserro), Toast.LENGTH_LONG).show();
                toLogin();
            }
        });
    }

    private void toLogin(){
        Intent intent = new Intent(WelcomeActivity.this,LoginActvity.class);
        startActivity(intent);
        this.finish();
    }
}
