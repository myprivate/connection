//package example.personal.connection.activity;
//
//import android.app.Activity;
//import android.content.Intent;
//import android.os.Bundle;
//import android.view.Gravity;
//import android.view.View;
//import android.view.Window;
//import android.view.WindowManager;
//import android.widget.LinearLayout;
//import android.widget.NumberPicker;
//import android.widget.TextView;
//
//import java.util.Calendar;
//
//import example.personal.connection.R;
//
///**
// * Created by wuqian on 2017/2/27.
// * mail: wuqian@ilingtong.com
// * Description:
// */
//public class GraduationPickerDialog extends Activity implements NumberPicker.OnValueChangeListener {
//    private NumberPicker numberPicker;
//    private TextView txt_ok;
//    private int year;
//
//    public static void launch(Activity activity,int requestCode){
//        Intent intent = new Intent(activity,GraduationPickerDialog.class);
//        activity.startActivityForResult(intent,requestCode);
//    }
//
//    @Override
//    protected void onCreate(Bundle savedInstanceState) {
//        super.onCreate(savedInstanceState);
//        requestWindowFeature(Window.FEATURE_NO_TITLE);
//        Window window = this.getWindow();
//        setContentView(R.layout.graduation_pick_layout);
//        window.getDecorView().setPadding(0, 0, 0, 0);
//        WindowManager.LayoutParams params = getWindow().getAttributes();
//        params.width = LinearLayout.LayoutParams.MATCH_PARENT;
//        params.height = LinearLayout.LayoutParams.WRAP_CONTENT;
//        params.gravity = Gravity.CENTER;
//        window.setAttributes(params);
//        init();
//    }
//
//    private void init() {
//        numberPicker = (NumberPicker) findViewById(R.id.graduation_picker);
//        txt_ok = (TextView) findViewById(R.id.graduation_txt_ok);
//        txt_ok.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                Intent intent = new Intent();
//                intent.putExtra("year",year+"");
//                setResult(RESULT_OK,intent);
//                GraduationPickerDialog.this.finish();
//            }
//        });
//        Calendar calendar = Calendar.getInstance();
//        numberPicker.setMaxValue(calendar.get(Calendar.YEAR));
//        numberPicker.setMinValue(calendar.get(Calendar.YEAR) - 100);
//        numberPicker.setValue(calendar.get(Calendar.YEAR));
//        year = calendar.get(Calendar.YEAR);
//        numberPicker.setOnValueChangedListener(this);
//    }
//
//    @Override
//    public void onValueChange(NumberPicker numberPicker, int oldNum, int newNum) {
//        year = newNum;
//    }
//}
