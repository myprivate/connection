package example.personal.connection.Utils;

import android.content.Context;
import android.view.WindowManager;

import example.personal.connection.MyApplication;

/**
 * Project: SHLTProject
 * Package: com.ilingtong.app.cedrus.utils
 * Description:单位换算类
 * author: liuting
 * date: 2016/8/10 10:26
 */
public class DipUtils {
    /**
     * 根据手机的分辨率从 px(像素) 的单位 转成为 dp
     *
     * @param pxValue
     * @return
     */
    public static int pxToDip(float pxValue) {
        return (int) (0.5F + pxValue * MyApplication.getInstance().getApplicationContext().getResources().getDisplayMetrics().density);
    }

    /**
     * 获取屏幕宽度
     *
     * @param paramContext
     * @return
     */
    public static int getScreenWidth(Context paramContext) {
        return ((WindowManager) paramContext.getSystemService("window")).getDefaultDisplay().getWidth();
    }

    /**
     * 根据手机的分辨率从 dip 的单位 转成为 px(像素)
     */
    public static int dip2px(Context paramContext, float dipValue) {
        return (int) (0.5F + dipValue * paramContext.getResources().getDisplayMetrics().density);
    }
}
