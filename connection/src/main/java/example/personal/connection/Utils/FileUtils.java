package example.personal.connection.Utils;

import android.os.Environment;


import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.Date;

import example.personal.connection.Constants;
import example.personal.connection.MyApplication;
import org.apache.http.util.EncodingUtils;

/**
 * Created by wuqian on 2016/10/13.
 * mail: wuqian@ilingtong.com
 * Description:文件工具类
 */
public class FileUtils {
    public static File createImageFile() {
        // Create an image file name
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        String imageFileName = "JPEG_" + timeStamp + "_";
        try {
            File imageFile = File.createTempFile(imageFileName,  /* prefix */
                    ".jpg",         /* suffix */
                    Environment.getExternalStorageDirectory()      /* directory */);
            return imageFile;
        } catch (IOException e) {
            //do noting
            return null;
        }
    }

    /**
     * 将拍下来的照片存放在SD卡中
     *
     * @param data
     * @throws IOException
     */
    public static void saveToSDCard(byte[] data) throws IOException {
        Date date = new Date();
        SimpleDateFormat format = new SimpleDateFormat("yyyyMMddHHmmss"); // 格式化时间
        String filename = format.format(date) + ".jpg";
        File fileFolder = new File(Environment.getExternalStorageDirectory()
                + Constants.FILE_FOLDER_NAME);
        if (!fileFolder.exists()) { // 如果目录不存在，则创建一个名为"finger"的目录
            fileFolder.mkdir();
        }
        File jpgFile = new File(fileFolder, filename);
        FileOutputStream outputStream = new FileOutputStream(jpgFile); // 文件输出流
        outputStream.write(data); // 写入sd卡中
        outputStream.close(); // 关闭输出流
    }

    public static File getFile(String filename) {
        String storageState = Environment.getExternalStorageState();// 获取sd卡的状态
        File file;
        File dir;
        if (Environment.MEDIA_MOUNTED.equals(storageState)) {// 如果已挂载状态
            //存储在SD卡文件夹image下
            dir = new File(Environment.getExternalStorageDirectory().getPath() + Constants.FILE_FOLDER_NAME);
            if (!dir.exists()) {
                dir.mkdirs();
            }

            file = new File(Environment.getExternalStorageDirectory().getPath() + Constants.FILE_FOLDER_NAME + "/" + filename);
            if (!file.exists()) {
                try {
                    file.createNewFile();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        } else {
            //存储在本地文件夹image下
            dir = new File(Constants.FILE_FOLDER_NAME);
            if (!dir.exists()) {
                dir.mkdirs();
            }
            file = new File(Constants.FILE_FOLDER_NAME + "/" + filename);
            if (!file.exists()) {
                try {
                    file.createNewFile();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        return file;
    }

    /**
     * 从resource的asset中读取文件数据
     * @param fileName
     * @return
     */
    public static  String readFile(String fileName) {
        String res = "";
        try {

            //得到资源中的asset数据流
            InputStream in = MyApplication.getInstance().getApplicationContext().getResources().getAssets().open(fileName);

            int length = in.available();
            byte[] buffer = new byte[length];

            in.read(buffer);
            in.close();
            res = EncodingUtils.getString(buffer, "UTF-8");

        } catch (Exception e) {

            e.printStackTrace();

        }
        return res;
    }
}
