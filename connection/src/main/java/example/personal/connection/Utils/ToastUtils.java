package example.personal.connection.Utils;

import android.content.Context;
import android.widget.Toast;

/**
 * Created by wuqian on 2016/9/12.
 * mail: wuqian@ilingtong.com
 * Description:Toast显示工具类
 */
public final class ToastUtils {

    /** 之前显示的内容 */
    private static String oldMsg ;
    /** Toast对象 */
    private static Toast toast = null ;
    /** 第一次时间 */
    private static long oneTime = 0 ;
    /** 第二次时间 */
    private static long twoTime = 0 ;

    /**
     * 显示Toast
     * @param context
     * @param message
     */
    public static void showToast(Context context,String message,int duration){
        if(toast == null){
            toast = Toast.makeText(context, message,duration);
            toast.show() ;
            oneTime = System.currentTimeMillis() ;
        }else{
            twoTime = System.currentTimeMillis() ;
            if(message.equals(oldMsg)){
                if(twoTime - oneTime > duration){
                    toast.show() ;
                }
            }else{
                oldMsg = message ;
                toast.setText(message) ;
                toast.show() ;
            }
        }
        oneTime = twoTime ;
    }

    /**
     * 显示默认的长时间显示
     * @param context
     * @param msg
     */
    public static void showLongToast(Context context,String msg){
        showToast(context,msg,Toast.LENGTH_LONG);
    }
    public static void showLongToast(Context context,int resId){
        showToast(context,context.getString(resId),Toast.LENGTH_LONG);
    }

    /**
     * 显示默认的短时间显示
     * @param context
     * @param msg
     */
    public static void showShortToast(Context context,String msg){
        showToast(context,msg,Toast.LENGTH_SHORT);
    }
    public static void showShortToast(Context context,int resId){
        showToast(context,context.getString(resId),Toast.LENGTH_SHORT);
    }
}
