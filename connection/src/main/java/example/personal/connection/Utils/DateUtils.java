package example.personal.connection.Utils;

import android.text.TextUtils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @author sunjichang
 * @version 1.0
 * @date 2017/3/4 09:09
 * @upate
 */

public class DateUtils {
    /**
     * 根据给定的日期字符串返回一个long数据
     * @param date
     * @return
     */
    public static long parseFormatGMTLongDate(String date){
        if(TextUtils.isEmpty(date)){
            return 0;
        }
        long time = 0;
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        if(isLongDateFormat(date)){
            df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        }else if(isShortDateFormat(date)){
            df = new SimpleDateFormat("yyyy-MM-dd");
        }else if(CommonUtils.isNumber(date)){
            return Long.parseLong(date);
        }else if(isLongDateFormatNoSS(date)){
            df = new SimpleDateFormat("yyyy-MM-dd HH:mm");
        }else{
            df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss Z", Locale.ENGLISH);
        }
        try {
            Date d = df.parse(date);
            time = d.getTime();
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return time;
    }

    /**
     * 判断当前的日期是否是长日期格式(yyyy-MM-dd HH:mm:ss)
     * @param date
     * @return
     */
    public static boolean isLongDateFormat(String date){
        if(TextUtils.isEmpty(date)){
            return false;
        }
        Pattern p = Pattern.compile("^\\d{4}-\\d{2}-\\d{2} \\d{2}:\\d{2}:\\d{2}$");
        Matcher m = p.matcher(date);
        return m.matches();
    }

    /**
     * 判断当前的日期是否是没有秒的长日期格式(yyyy-MM-dd HH:mm)
     * @param date
     * @return
     */
    public static boolean isLongDateFormatNoSS(String date){
        if(TextUtils.isEmpty(date)){
            return false;
        }
        Pattern p = Pattern.compile("^\\d{4}-\\d{2}-\\d{2} \\d{2}:\\d{2}$");
        Matcher m = p.matcher(date);
        return m.matches();
    }

    /**
     * 判断当前日期格式是否是简易日期格式(即：yyyy-MM-dd)
     * @param date
     * @return
     */
    public static boolean isShortDateFormat(String date){
        if(TextUtils.isEmpty(date)){
            return false;
        }
        Pattern p = Pattern.compile("^\\d{4}-\\d{2}-\\d{2}$");
        Matcher m = p.matcher(date);
        return m.matches();
    }

    public static String getChatTime(long time){
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyyMMddHHmmss");
        Date curDate = new Date(time);
        String showTime = null;
        boolean b = isToday(curDate);
        if (b) {
			/*SimpleDateFormat simFormat1 = new SimpleDateFormat("HHmm");
			int n = Integer.parseInt(simFormat1.format(curDate));
			if (n > 0000 && n < 1159) {
				SimpleDateFormat simFormat = new SimpleDateFormat("上午 hh:mm");
				showTime = simFormat.format(curDate);
			}else {
				SimpleDateFormat simFormat = new SimpleDateFormat("下午 hh:mm");
				showTime = simFormat.format(curDate);
			}*/
            SimpleDateFormat simFormat = new SimpleDateFormat("HH:mm");
            showTime = simFormat.format(curDate);

        }else {
            SimpleDateFormat simFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            showTime = simFormat.format(curDate);
        }
        return showTime;
    }

    /**
     * 是否是今天
     * @param date
     * @return
     */
    public static boolean isToday(final Date date) {
        return isTheDay(date, now());
    }
    private static Date now() {
        // TODO Auto-generated method stub
        Date curDate = new Date(System.currentTimeMillis());
        return curDate;
    }
    /**
     * 是否是指定日期
     *
     * @param date
     * @param day
     * @return
     */
    public static boolean isTheDay(final Date date, final Date day) {
        return date.getTime() >= dayBegin(day).getTime()
                && date.getTime() <= dayEnd(day).getTime();
    }

    /**
     * 获取指定时间的那天 00:00:00.000 的时间
     *
     * @param date
     * @return
     */
    public static Date dayBegin(final Date date) {
        Calendar c = Calendar.getInstance();
        c.setTime(date);
        c.set(Calendar.HOUR_OF_DAY, 0);
        c.set(Calendar.MINUTE, 0);
        c.set(Calendar.SECOND, 0);
        c.set(Calendar.MILLISECOND, 0);
        return c.getTime();
    }
    /**
     * 获取指定时间的那天 23:59:59.999 的时间
     *
     * @param date
     * @return
     */
    public static Date dayEnd(final Date date) {
        Calendar c = Calendar.getInstance();
        c.setTime(date);
        c.set(Calendar.HOUR_OF_DAY, 23);
        c.set(Calendar.MINUTE, 59);
        c.set(Calendar.SECOND, 59);
        c.set(Calendar.MILLISECOND, 999);
        return c.getTime();
    }
}
