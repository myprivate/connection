package example.personal.connection.Utils;

import android.content.Context;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Handler;
import android.os.Message;
import android.provider.MediaStore;
import android.util.Base64;

import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.assist.ImageScaleType;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.mime.HttpMultipartMode;
import org.apache.http.entity.mime.MultipartEntityBuilder;
import org.apache.http.entity.mime.content.ByteArrayBody;
import org.apache.http.entity.mime.content.FileBody;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.CharsetUtils;
import org.apache.http.util.EntityUtils;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import example.personal.connection.R;

/**
 * Created by wuqian on 2017/2/27.
 * mail: wuqian@ilingtong.com
 * Description:
 */
public class CommonUtils {
    //将Bitmap转换成字符串
    public static String bitmaptoString(Bitmap bitmap) {
        String string = null;
        ByteArrayOutputStream bStream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.PNG, 100, bStream);
        byte[] bytes = bStream.toByteArray();
        string = Base64.encodeToString(bytes, Base64.DEFAULT);
        return string;
    }

    /**
     * 获取头像图片opitions
     *
     * @return
     */
    public static DisplayImageOptions getUserIconOptions() {
        return new DisplayImageOptions.Builder()
                .showStubImage(R.mipmap.default_head_icon)            // 设置图片下载期间显示的图片
                .showImageForEmptyUri(R.mipmap.default_head_icon)    // 设置图片Uri为空或是错误的时候显示的图片
                .showImageOnFail(R.mipmap.default_head_icon)        // 设置图片加载或解码过程中发生错误显示的图片
                .cacheInMemory(true)                        // 设置下载的图片是否缓存在内存中
                .cacheOnDisc(true)                            // 设置下载的图片是否缓存在SD卡中
                .bitmapConfig(Bitmap.Config.RGB_565)
                .imageScaleType(ImageScaleType.IN_SAMPLE_INT)
                .build();
    }

    /**
     * 获取当前时间
     *
     * @return
     */
    public static String getTime() {
        return new SimpleDateFormat("MM-dd HH:mm", Locale.CHINA).format(new Date());
    }

    public static boolean isEmpty(String str) {
        return null == str || "".equals(str) || "null".equals(str);
    }

    /**
     * 判断当前给定的字符是不是全部由数字组成
     *
     * @param number
     * @return
     */
    public static boolean isNumber(String number) {
        if (isEmpty(number)) {
            return false;
        }
        Pattern p = Pattern.compile("^(-)?[0-9]*[0-9]$");
        Matcher m = p.matcher(number);
        return m.find();
    }

    /**
     * 将bitmap转化为byte数组
     *
     * @param bm
     * @return
     */
    public static byte[] Bitmap2Bytes(Bitmap bm) {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        bm.compress(Bitmap.CompressFormat.PNG, 100, baos);
        return baos.toByteArray();
    }

    /**
     * 上传图片
     *
     * @param url
     * @return
     */
    public static void uploadImage(String url, Bitmap bitmap, Handler handler) {
        String result = "";
        try {
            HttpClient client = new DefaultHttpClient(); // 1.创建httpclient对象
            HttpPost post = new HttpPost(url); // 2.通过url创建post方法
            post.setHeader("accept", "application/json");

            // ***************************************<向post方法中封装实体>************************************//3.向post方法中封装实体
            MultipartEntityBuilder builder = MultipartEntityBuilder.create(); // 实例化实体构造器
            builder.setMode(HttpMultipartMode.BROWSER_COMPATIBLE); // 设置浏览器兼容模式
            ByteArrayBody bab = new ByteArrayBody(Bitmap2Bytes(bitmap), "pic.png");
            builder.addPart("file", bab); // 添加"file"字段及其值

            HttpEntity reqEntity = builder.setCharset(CharsetUtils.get("UTF-8")).build(); // 设置请求的编码格式，并构造实体

            post.setEntity(reqEntity);
            // **************************************</向post方法中封装实体>************************************

            HttpResponse response = client.execute(post); // 4.执行post方法，返回HttpResponse的对象
            if (response.getStatusLine().getStatusCode() == 200) { // 5.如果返回结果状态码为200，则读取响应实体response对象的实体内容，并封装成String对象返回
                result = EntityUtils.toString(response.getEntity(), "UTF-8");
                Message mes = new Message();
                mes.obj = result;
                mes.what = 0;
                handler.sendMessage(mes);

            } else {
                Message mes = new Message();
                mes.obj = "服务器返回异常";
                mes.what = 1;
                handler.sendMessage(mes);
            }
            try {
                HttpEntity e = response.getEntity(); // 6.关闭资源
                if (e != null) {
                    InputStream instream = e.getContent();
                    instream.close();
                }
            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                ((InputStream) response).close();
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
        }
    }
    /**
     * 上传图片
     *
     * @param url
     * @return
     */
    public static void uploadImageFile(String url, File file, Handler handler) {
        String result = "";
        try {
            HttpClient client = new DefaultHttpClient(); // 1.创建httpclient对象
            HttpPost post = new HttpPost(url); // 2.通过url创建post方法
            post.setHeader("accept", "application/json");

            // ***************************************<向post方法中封装实体>************************************//3.向post方法中封装实体
            MultipartEntityBuilder builder = MultipartEntityBuilder.create(); // 实例化实体构造器
            builder.setMode(HttpMultipartMode.BROWSER_COMPATIBLE); // 设置浏览器兼容模式
            builder.addPart("file", new FileBody(file)); // 添加"file"字段及其值

            HttpEntity reqEntity = builder.setCharset(CharsetUtils.get("UTF-8")).build(); // 设置请求的编码格式，并构造实体

            post.setEntity(reqEntity);
            // **************************************</向post方法中封装实体>************************************

            HttpResponse response = client.execute(post); // 4.执行post方法，返回HttpResponse的对象
            if (response.getStatusLine().getStatusCode() == 200) { // 5.如果返回结果状态码为200，则读取响应实体response对象的实体内容，并封装成String对象返回
                result = EntityUtils.toString(response.getEntity(), "UTF-8");
                Message mes = new Message();
                mes.obj = result;
                mes.what = 0;
                handler.sendMessage(mes);

            } else {
                Message mes = new Message();
                mes.obj = "服务器返回异常";
                mes.what = 1;
                handler.sendMessage(mes);
            }
            try {
                HttpEntity e = response.getEntity(); // 6.关闭资源
                if (e != null) {
                    InputStream instream = e.getContent();
                    instream.close();
                }
            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                ((InputStream) response).close();
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
        }
    }
    /**
     * 根据路径获得图片并压缩返回bitmap用于显示
     *
     * @param filePath
     * @return
     */
    public static Bitmap getSmallBitmap(String filePath, int reqWidth, int reqHeight) {
        final BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;  //只返回图片的大小信息
        BitmapFactory.decodeFile(filePath, options);
        // Calculate inSampleSize
        options.inSampleSize = calculateInSampleSize(options, reqWidth, reqHeight);
        // Decode bitmap with inSampleSize set
        options.inJustDecodeBounds = false;

        return BitmapFactory.decodeFile(filePath, options);
    }
    /**
     * 计算图片的缩放值
     *
     * @param options
     * @param reqWidth
     * @param reqHeight
     * @return
     */
    public static int calculateInSampleSize(BitmapFactory.Options options,
                                            int reqWidth, int reqHeight) {
        // Raw height and width of image
        final int height = options.outHeight;
        final int width = options.outWidth;
        int inSampleSize = 1;

        if (height > reqHeight || width > reqWidth) {

            // Calculate ratios of height and width to requested height and
            // width
            final int heightRatio = Math.round((float) height
                    / (float) reqHeight);
            final int widthRatio = Math.round((float) width / (float) reqWidth);

            // Choose the smallest ratio as inSampleSize value, this will
            // guarantee
            // a final image with both dimensions larger than or equal to the
            // requested height and width.
            inSampleSize = heightRatio < widthRatio ? heightRatio : widthRatio;
        }

        return inSampleSize;
    }
    /**
     * 获取图片真实路径
     *
     * @param context
     * @param contentURI
     * @return
     */
    public static String getRealPathFromURI(Context context, Uri contentURI) {
        String result;
        Cursor cursor = context.getContentResolver().query(contentURI, null, null, null, null);
        if (cursor == null) {
            // Source is Dropbox or other similar local file path
            result = contentURI.getPath();
        } else {
            cursor.moveToFirst();
            int idx = cursor.getColumnIndex(MediaStore.Images.ImageColumns.DATA);
            result = cursor.getString(idx);
            cursor.close();
        }
        return result;
    }
}
