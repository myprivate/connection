package example.personal.connection.widget;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import example.personal.connection.R;

/**
 * @author sunjichang
 * @version 1.0
 * @date 2017/3/5 22:23
 * @upate
 */

public class PullLoadListView extends ListView{
    private View mHeaderView = null;
    private int mHeaderHeight = 0;
    private float mDownY = 0;
    private boolean allowPull = false;
    private PullState mState = PullState.NONE;
    private boolean enableLoad = true;
    private PullLoadListener mListener = null;
    private enum PullState{
        NONE,PULL,LOADING
    }
    public PullLoadListView(Context context) {
        super(context);
        initView(context);
    }

    public PullLoadListView(Context context, AttributeSet attrs) {
        super(context, attrs);
        initView(context);
    }

    public PullLoadListView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        initView(context);
    }

    private void initView(Context context){
        LayoutInflater inflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        mHeaderView = inflater.inflate(R.layout.layout_pull_load_listview_head,null);
        measureView(mHeaderView);
        mHeaderHeight = mHeaderView.getMeasuredHeight() + 30;
        topPadding(-mHeaderHeight);
        this.addHeaderView(mHeaderView);
    }

    /**
     * 设置顶部布局的上边距
     * @param topPadding
     */
     private void topPadding(int topPadding){
        //设置顶部提示的边距//除了顶部用参数值topPadding外，其他三个用header默认的值
         mHeaderView.setPadding(mHeaderView.getPaddingLeft(), topPadding,
                 mHeaderView.getPaddingRight(), mHeaderView.getPaddingBottom());
        //使header无效，将来调用onDraw()重绘View
         mHeaderView.invalidate();
     }

    /**
     * 测量view的高度
     * @param view
     */
    private void measureView(View view){
        //LayoutParams are used by views to tell their parents
        //how they want to be laid out.
        //LayoutParams被view用来告诉它们的父布局它们应该被怎样安排
        ViewGroup.LayoutParams p = view.getLayoutParams();
        if(p==null){
            //两个参数:width,height
            p = new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.WRAP_CONTENT);
        }
        //getChildMeasureSpec:获取子View的widthMeasureSpec、heightMeasureSpec值
        int width = ViewGroup.getChildMeasureSpec(0, 0, p.width);
        int height;
        int tempHeight = p.height;
        if(tempHeight>0){
            height = MeasureSpec.makeMeasureSpec(tempHeight, MeasureSpec.EXACTLY);
        }else{
            height = MeasureSpec.makeMeasureSpec(0, MeasureSpec.UNSPECIFIED);
        }
        view.measure(width, height);
    }

    @Override
    public boolean onTouchEvent(MotionEvent ev) {
        int action = ev.getAction() & MotionEvent.ACTION_MASK;
        switch (action){
            case MotionEvent.ACTION_DOWN:
                mDownY = ev.getY();
                allowPull = getFirstVisiblePosition() == 0;
                break;
            case MotionEvent.ACTION_MOVE:
                move(ev);
                break;
            case MotionEvent.ACTION_UP:
                up(ev);
                break;
        }
        return super.onTouchEvent(ev);
    }

    private void move(MotionEvent ev){
        if(!allowPull || !enableLoad){
            return;
        }
        float y = ev.getY();
        float distance = y - mDownY;
        float space = distance - mHeaderHeight;
        if(space < 0){
            mState = PullState.PULL;
        }else{
            mState = PullState.LOADING;
        }
        topPadding(space > 0 ? 0 : (int)space);
    }

    private void up(MotionEvent ev){
        float distance = ev.getY() - mDownY;
        float space = distance - mHeaderHeight;
        switch (mState){
            case NONE:
            case PULL:
                topPadding(-mHeaderHeight);
                break;
            case LOADING:
                if(null != mListener){
                    mListener.loading();
                }
                break;
        }
    }

    /**
     * 加载完成
     */
    public void loadComplete(){
        topPadding(-mHeaderHeight);
    }

    /**
     * 设置是否可以下拉加载
     * @param enable
     */
    public void enableLoad(boolean enable){
        enableLoad = enable;
    }

    public void setLoadingListener(PullLoadListener listener){
        this.mListener = listener;
    }

    public interface PullLoadListener{
        public void loading();
    }
}
