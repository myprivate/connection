package example.personal.connection.entity;

import java.util.List;

/**
 * Created by wuqian on 2017/2/28.
 * mail: wuqian@ilingtong.com
 * Description: 获取我的联系人返回实体
 */
public class ContactResult extends BaseBean{
    public List<LoginInfoBean> data;
    //好友申请个数
    public int extraData;
}
