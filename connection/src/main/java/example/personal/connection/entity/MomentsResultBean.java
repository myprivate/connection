package example.personal.connection.entity;

import java.util.List;

/**
 * Created by wuqian on 2017/2/28.
 * mail: wuqian@ilingtong.com
 * Description:我的朋友圈json返回实体
 */
public class MomentsResultBean extends BaseBean{

    public MomentsListBean data;

    public static class MomentsListBean{
        public int totalPage;
        public int pageSize;
        public int pageNum;
        public boolean lastPage;
        public List<MomentsBean> data;
    }

    public static class MomentsBean{
        /**
         * 数据库记录id
         */
        public String id;
        /**
         * 姓名
         */
        public String name;
        /**
         * 头像
         */
        public String photo;
        /**
         * 文字内容
         */
        public String contentText;
        /**
         * 图片内容
         */
        public String contentPicture;
        /**
         * 创建时间
         */
        public String createTime;
    }
}
