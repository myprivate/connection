package example.personal.connection.entity;

/**
 * @author sunjichang
 * @version 1.0
 * @date 2017/3/5 14:56
 * @upate
 */

public class FriendApplyBean {
    /**
     * 记录id
     */
    public String id;
    /**
     * 好友姓名
     */
    public String friendName;
    /**
     * 好友头像
     */
    public String friendPhoto;
    /**
     * 申请内容
     */
    public String applyContent;

    /**
     * 创建时间
     */
    public String createTime;
}
