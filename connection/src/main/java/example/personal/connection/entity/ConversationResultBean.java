package example.personal.connection.entity;

import java.util.List;

/**
 * @author sunjichang
 * @version 1.0
 * @date 2017/3/1 22:00
 * @upate
 */

public class ConversationResultBean extends BaseBean {

    public List<ConversationBean> data = null;

    public static class ConversationBean{
        public String id;
        public String userId;
        public String friendId;
        public String friendName;
        public String friendPhoto;
        public String creatTime;
        public String friendVoipUserName;
        public String modifyTime;
    }
}
