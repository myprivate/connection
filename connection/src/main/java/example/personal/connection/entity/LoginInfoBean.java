package example.personal.connection.entity;

import java.io.Serializable;

/**
 * Created by wuqian on 2017/2/27.
 * mail: wuqian@ilingtong.com
 * Description: 登录返回信息
 */
public class LoginInfoBean implements Serializable {

    public String id;
    public String phone;
    public String name;
    public String email;
    public String city;
    public String university;
    public String yearOfGraduation;
    public String major;
    public String classNumber;
    public String photo;
    public String voipId;
    public String voipUsername;
    public String voipPwd;
    public String voipNickname;
    public String birthday;

}
