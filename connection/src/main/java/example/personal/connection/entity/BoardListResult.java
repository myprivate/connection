package example.personal.connection.entity;

import java.io.Serializable;
import java.util.List;

/**
 * Created by wuqian on 2017/2/28.
 * mail: wuqian@ilingtong.com
 * Description:获取公告列表json返回实体
 */
public class BoardListResult extends BaseBean implements Serializable{

    public BoardListBean data;

    public static class BoardListBean implements Serializable{
        public int totalPage;
        public int pageSize;
        public int pageNum;
        public boolean lastPage;
        public List<BoardBean> data;
    }


    public static class BoardBean implements Serializable{
        /**
         * 记录id
         */
        public String id;
        /**
         * 标题
         */
        public String title;
        /**
         * 内容
         */
        public String content;
        /**
         * 创建时间
         */
        public String createTime;
        /**
         * 操作人员id
         */
        public String operateId;
    }
}
