package example.personal.connection.entity;

import java.io.Serializable;

/**
 * Created by wuqian on 2017/3/2.
 * mail: wuqian@ilingtong.com
 * Description: 修改个人信息入口参数实体
 * 方便值的传递，无其他意义
 */
public class RequestUserInfoBean implements Serializable{
    public String id;
    public String name;
    public String email;
    public String city;
    public String university;
    public String yearOfGraduation;
    public String major;
    public String classNumber;
    public String photo;
    public String birthday;
    public String phone;
}
