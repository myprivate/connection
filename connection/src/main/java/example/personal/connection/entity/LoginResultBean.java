package example.personal.connection.entity;

import java.io.Serializable;

/**
 * Created by wuqian on 2017/2/27.
 * mail: wuqian@ilingtong.com
 * Description:登录接口返回实体
 */
public class LoginResultBean extends BaseBean implements Serializable {
    public LoginInfoBean data;
}
