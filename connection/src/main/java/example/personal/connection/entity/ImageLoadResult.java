package example.personal.connection.entity;

/**
 * Created by wuqian on 2017/3/4.
 * mail: wuqian@ilingtong.com
 * Description: 图片上传返回json实体
 */
public class ImageLoadResult extends BaseBean{
    public String data;
}
