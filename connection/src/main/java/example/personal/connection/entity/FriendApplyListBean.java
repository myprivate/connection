package example.personal.connection.entity;

import java.util.List;

/**
 * @author sunjichang
 * @version 1.0
 * @date 2017/3/5 14:57
 * @upate
 */

public class FriendApplyListBean extends BaseBean{
    public List<FriendApplyBean> data;
}
