package example.personal.connection.entity;

import java.io.Serializable;

/**
 * Created by wuqian on 2017/2/24.
 * mail: wuqian@ilingtong.com
 * Description:接口返回的实体父类
 */
public class BaseBean implements Serializable{
    public String code;
    public String message;
}
