package example.personal.connection.entity;

import java.io.Serializable;

/**
 * Created by wuqian on 2017/2/27.
 * mail: wuqian@ilingtong.com
 * Description:注册接口返回实体
 */
public class RegisterResultBean extends BaseBean implements Serializable{
    public LoginInfoBean data;
}
