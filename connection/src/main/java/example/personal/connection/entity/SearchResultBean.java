package example.personal.connection.entity;

import java.io.Serializable;
import java.util.List;

/**
 * Created by wuqian on 2017/2/28.
 * mail: wuqian@ilingtong.com
 * Description:搜索校友json返回实体
 */
public class SearchResultBean extends BaseBean implements Serializable{

    public SearchListBean data;

    public static class SearchListBean implements Serializable{
        public int totalPage;
        public int pageSize;
        public int pageNum;
        public boolean lastPage;
        public List<LoginInfoBean> data;
    }
}
