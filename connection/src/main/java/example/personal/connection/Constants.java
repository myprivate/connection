package example.personal.connection;

/**
 * Created by wuqian on 2017/2/27.
 * mail: wuqian@ilingtong.com
 * Description:
 */
public class Constants {
    //    public static String URL = "https://connection.applinzi.com/";
//      public static String URL = "http://192.168.1.6:8080/connection/";
    //华为服务器地址
//    public static String URL = "http://139.159.215.183:8080/connection/";
    //hostus 服务器地址
    public static String URL = "http://202.5.16.149/connection/";
    public static String LOGIN_URL = URL + "user/login";
    public static String REGISTER_URL = URL + "user/registerOneTime";
    public static String GETCONTACT_URL = URL + "friend/queryFriend";
    public static String GET_MYPOST_URL = URL + "moment/querySelfMoment";   //获取自己发的朋友圈
    public static String GET_MOMENTS_URL = URL + "moment/queryMoment";     //获取朋友圈
    public static String SEND_MOMENT_URL = URL + "moment/sendMoment";   //发朋友圈
    public static String SEARCH_FIREND_URL = URL + "user/searchByKeyword";    //搜索校友
    public static String ADD_FIREND_URL = URL + "friend/friendApply";   //发起校友添加申请
    public static String GET_USERINFO_URL = URL + "user/query";   //查询个人信息
    public static String AGRENN_APPLY_URL = URL + "friend/agreeApply";   //同意添加朋友
    public static String UPDATE_USERINFO_URL = URL + "user/update";   //修改个人信息
    public static String GET_BORAD_LIST_URL = URL + "bulletin/queryAll";  //获取公告列表
    public static String PUBLISH_BORAD = URL + "bulletin/add";    //发布公告
    public static String GET_CONVERSATION = URL + "conversation/queryByUserId";//查询会话列表
    public static String ADD_CONVERSATION = URL + "conversation/add";//添加会话列表
    public static String IMAGE_UPLOAD = URL + "file/uploadImage";//添加会话列表
    public static String GET_APPLY_LIST = URL + "friend/queryFriendApply";   //获取好友申请列表
    public static String DELETE_FRIEND_URL = URL + "friend/delete";    //删除好友

    public static String SUCCESSCODE = "2000";
    public static final String FILE_FOLDER_NAME = "/connection";

    public static final int PAGE_SIZE = 10; //一页十条

    public static String formatImageUrl(String url){
        return URL+url;
    }
}
