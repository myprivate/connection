package example.personal.connection.manager;

import com.android.volley.Request;
import com.android.volley.Response;

import java.util.HashMap;
import java.util.Map;

import example.personal.connection.Constants;
import example.personal.connection.MyApplication;
import example.personal.connection.entity.BaseBean;
import example.personal.connection.entity.BoardListResult;
import example.personal.connection.entity.ContactResult;
import example.personal.connection.entity.ConversationResultBean;
import example.personal.connection.entity.FriendApplyListBean;
import example.personal.connection.entity.LoginResultBean;
import example.personal.connection.entity.MomentsResultBean;
import example.personal.connection.entity.RegisterResultBean;
import example.personal.connection.entity.RequestUserInfoBean;
import example.personal.connection.entity.SearchResultBean;

/**
 * Created by wuqian on 2017/2/27.
 * mail: wuqian@ilingtong.com
 * Description:
 */
public class ServiceManager {
    /**
     * 登录接口
     *
     * @param phone
     * @param passwrod
     * @param listener
     * @param errorListener
     */
    public static void doLogin(String phone, String passwrod, Response.Listener listener, Response.ErrorListener errorListener) {

        Map<String, String> param = new HashMap<>();
        param.put("loginId", phone);
        param.put("password", passwrod);
        GsonRequest<LoginResultBean> request = new GsonRequest<LoginResultBean>(Request.Method.POST, Constants.LOGIN_URL, LoginResultBean.class, param, listener, errorListener);
        MyApplication.getInstance().getRequestQueue().add(request);
    }

    /**
     * 注册接口
     *
     * @param name
     * @param birthday
     * @param email
     * @param city
     * @param university
     * @param yearOfGraduation
     * @param major
     * @param classNumber
     * @param phone
     * @param password
     * @param photo
     * @param listener
     * @param errorListener
     */
    public static void doRegister(String name, String birthday, String email,
                                  String city, String university, String yearOfGraduation,
                                  String major, String classNumber, String phone, String password, String photo, Response.Listener listener, Response.ErrorListener errorListener) {
        Map<String, String> param = new HashMap<>();
        param.put("name", name);
        param.put("birthday", birthday);
        param.put("email", email);
        param.put("city", city);
        param.put("university", university);
        param.put("yearOfGraduation", yearOfGraduation);
        param.put("major", major);
        param.put("classNumber", classNumber);
        param.put("phone", phone);
        param.put("password", password);
        param.put("photo", photo);

        GsonRequest<RegisterResultBean> request = new GsonRequest<RegisterResultBean>(Request.Method.POST, Constants.REGISTER_URL, RegisterResultBean.class, param, listener, errorListener);
        MyApplication.getRequestQueue().add(request);
    }

    /**
     * 获取我的校友
     *
     * @param userid
     * @param listener
     * @param errorListener
     */
    public static void getContactList(String userid, Response.Listener<ContactResult> listener, Response.ErrorListener errorListener) {
        Map<String, String> param = new HashMap<>();
        param.put("userId", userid);

        GsonRequest<ContactResult> request = new GsonRequest<>(Request.Method.POST, Constants.GETCONTACT_URL, ContactResult.class, param, listener, errorListener);
        MyApplication.getRequestQueue().add(request);
    }

    /**
     * 获取我自己发的朋友圈列表
     *
     * @param userid
     * @param pageSize
     * @param pageNum
     * @param listener
     * @param errorListener
     */
    public static void getMyPostList(String userid, int pageSize, int pageNum, Response.Listener<MomentsResultBean> listener, Response.ErrorListener errorListener) {

        Map<String, String> param = new HashMap<>();
        param.put("userId", userid);
        param.put("pageSize", pageSize + "");
        param.put("pageNum", pageNum + "");

        GsonRequest<MomentsResultBean> request = new GsonRequest<>(Request.Method.POST, Constants.GET_MYPOST_URL, MomentsResultBean.class, param, listener, errorListener);
        MyApplication.getRequestQueue().add(request);
    }

    /**
     * 获取我的朋友圈
     *
     * @param userid
     * @param pageSize
     * @param pageNum
     * @param listener
     * @param errorListener
     */
    public static void getMoments(String userid, int pageSize, int pageNum, Response.Listener<MomentsResultBean> listener, Response.ErrorListener errorListener) {

        Map<String, String> param = new HashMap<>();
        param.put("userId", userid);
        param.put("pageSize", pageSize + "");
        param.put("pageNum", pageNum + "");

        GsonRequest<MomentsResultBean> request = new GsonRequest<>(Request.Method.POST, Constants.GET_MOMENTS_URL, MomentsResultBean.class, param, listener, errorListener);
        MyApplication.getRequestQueue().add(request);
    }

    /**
     * 发朋友圈
     *
     * @param userid
     * @param content
     * @param pictrues
     * @param listener
     * @param errorListener
     */
    public static void sendMoment(String userid, String content, String pictrues, Response.Listener<BaseBean> listener, Response.ErrorListener errorListener) {
        Map<String, String> param = new HashMap<>();
        param.put("userId", userid);
        param.put("content", content);
        param.put("pictures", pictrues);

        GsonRequest<BaseBean> request = new GsonRequest<>(Request.Method.POST, Constants.SEND_MOMENT_URL, BaseBean.class, param, listener, errorListener);
        MyApplication.getRequestQueue().add(request);
    }

    /**
     * 根据关键字搜索校友
     *
     * @param userid
     * @param keyword
     * @param pageSize
     * @param pageNum
     * @param listener
     * @param errorListener
     */
    public static void searchByKeyword(String userid, String keyword, int pageSize, int pageNum, Response.Listener<SearchResultBean> listener, Response.ErrorListener errorListener) {
        Map<String, String> param = new HashMap<>();
        param.put("userId", userid);
        param.put("keyword", keyword);
        param.put("pageSize", pageSize + "");
        param.put("pageNum", pageNum + "");

        GsonRequest<SearchResultBean> request = new GsonRequest<>(Request.Method.POST, Constants.SEARCH_FIREND_URL, SearchResultBean.class, param, listener, errorListener);
        MyApplication.getRequestQueue().add(request);
    }

    /**
     * 发起添加好友申请
     *
     * @param applyId
     * @param friendId
     * @param applyContent
     * @param listener
     * @param errorListener
     */
    public static void friendApply(String applyId, String friendId, String applyContent, Response.Listener<BaseBean> listener, Response.ErrorListener errorListener) {
        Map<String, String> param = new HashMap<>();
        param.put("applyId", applyId);
        param.put("friendId", friendId);
        param.put("applyContent", applyContent);

        GsonRequest<BaseBean> request = new GsonRequest<>(Request.Method.POST, Constants.ADD_FIREND_URL, BaseBean.class, param, listener, errorListener);
        MyApplication.getRequestQueue().add(request);
    }

    /**
     * 根据userid获取个人信息
     *
     * @param userid
     * @param listener
     * @param errorListener
     */
    public static void getUserInfo(String userid, Response.Listener<LoginResultBean> listener, Response.ErrorListener errorListener) {
        Map<String, String> param = new HashMap<>();
        param.put("userId", userid);

        GsonRequest<LoginResultBean> request = new GsonRequest<>(Request.Method.POST, Constants.GET_USERINFO_URL, LoginResultBean.class, param, listener, errorListener);
        MyApplication.getRequestQueue().add(request);
    }

    /**
     * 同意好友申请
     *
     * @param id
     * @param listener
     * @param errorListener
     */
    public static void agreeApply(String id, Response.Listener<BaseBean> listener, Response.ErrorListener errorListener) {
        Map<String, String> param = new HashMap<>();
        param.put("id", id);

        GsonRequest<BaseBean> request = new GsonRequest<>(Request.Method.POST, Constants.AGRENN_APPLY_URL, BaseBean.class, param, listener, errorListener);
        MyApplication.getRequestQueue().add(request);
    }

    /**
     * 修改个人信息
     *
     * @param listener
     * @param errorListener
     */
    public static void updateUserInfo(RequestUserInfoBean paramBean, Response.Listener<BaseBean> listener, Response.ErrorListener errorListener) {
        Map<String, String> param = new HashMap<>();
        param.put("id", MyApplication.getInstance().getUserId());
        param.put("name", paramBean.name);
        param.put("photo", paramBean.photo);
        param.put("birthday", paramBean.birthday);
        param.put("email", paramBean.email);
        param.put("city", paramBean.city);
        param.put("university", paramBean.university);
        param.put("yearOfGraduation", paramBean.yearOfGraduation);
        param.put("major", paramBean.major);
        param.put("classNumber", paramBean.classNumber);

        GsonRequest<BaseBean> request = new GsonRequest<>(Request.Method.POST, Constants.UPDATE_USERINFO_URL, BaseBean.class, param, listener, errorListener);
        MyApplication.getRequestQueue().add(request);
    }

    /**
     * 获取公告列表
     *
     * @param pageSize
     * @param pageNum
     * @param listener
     * @param errorListener
     */
    public static void getBoradList(int pageSize, int pageNum, Response.Listener<BoardListResult> listener, Response.ErrorListener errorListener) {
        Map<String, String> param = new HashMap<>();
        param.put("pageSize", pageSize + "");
        param.put("pageNum", pageNum + "");

        GsonRequest<BoardListResult> request = new GsonRequest<>(Request.Method.POST, Constants.GET_BORAD_LIST_URL, BoardListResult.class, param, listener, errorListener);
        MyApplication.getRequestQueue().add(request);
    }

    /**
     * 发布公告
     */
    public static void publishBorad(String title, String content,Response.Listener<BaseBean> listener, Response.ErrorListener errorListener) {
        Map<String,String> param = new HashMap<>();
        param.put("title",title);
        param.put("content",content);
        param.put("operateId",MyApplication.getInstance().getUserId());

        GsonRequest<BaseBean> request = new GsonRequest<>(Request.Method.POST,Constants.PUBLISH_BORAD,BaseBean.class,param,listener,errorListener);
        MyApplication.getRequestQueue().add(request);
    }

    /**
     * 获取会话列表
     *
     * @param userId
     * @param listener
     * @param errorListener
     */
    public static void getConversationList(String userId, Response.Listener<ConversationResultBean> listener,
                                           Response.ErrorListener errorListener) {
        Map<String, String> param = new HashMap<>();
        param.put("userId", userId);
        GsonRequest<ConversationResultBean> request = new GsonRequest<>(Request.Method.POST, Constants.GET_CONVERSATION,
                ConversationResultBean.class, param, listener, errorListener);
        MyApplication.getRequestQueue().add(request);
    }

    /**
     * 添加会话列表
     * @param userId 用户id
     * @param friendId 好友id
     * @param listener
     * @param errorListener
     */
    public static void addConversation(String userId, String friendId, Response.Listener<BaseBean> listener,
                                       Response.ErrorListener errorListener){
        Map<String, String> param = new HashMap<>();
        param.put("userId", userId);
        param.put("friendId", friendId);
        GsonRequest<BaseBean> request = new GsonRequest<>(Request.Method.POST, Constants.ADD_CONVERSATION,
                BaseBean.class, param, listener, errorListener);
        MyApplication.getRequestQueue().add(request);
    }

    /**
     * 获取好友申请列表
     * @param userId
     * @param listener
     * @param errorListener
     */
    public static void getApplyList(String userId, Response.Listener<FriendApplyListBean> listener, Response.ErrorListener errorListener){
        Map<String,String> param = new HashMap<>();
        param.put("userId",userId);
        GsonRequest<FriendApplyListBean> request = new GsonRequest<>(Request.Method.POST,Constants.GET_APPLY_LIST,FriendApplyListBean.class,param,listener,errorListener);
        MyApplication.getRequestQueue().add(request);
    }

    /**
     * 删除好友
     * @param friendId
     * @param listener
     * @param errorListener
     */
    public static void deleteFriend(String friendId, Response.Listener<BaseBean> listener, Response.ErrorListener errorListener){
        Map<String,String> param = new HashMap<>();
        param.put("userId",MyApplication.getInstance().getUserId());
        param.put("friendId",friendId);

        GsonRequest<BaseBean> request = new GsonRequest<>(Request.Method.POST,Constants.DELETE_FRIEND_URL,BaseBean.class,param,listener,errorListener);
        MyApplication.getRequestQueue().add(request);
    }
}
