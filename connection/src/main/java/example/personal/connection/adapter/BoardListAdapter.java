package example.personal.connection.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.List;

import example.personal.connection.R;
import example.personal.connection.entity.BoardListResult;

/**
 * Created by wuqian on 2017/3/3.
 * mail: wuqian@ilingtong.com
 * Description:公告栏列表适配器
 */
public class BoardListAdapter extends BaseAdapter {
    private List<BoardListResult.BoardBean> list;
    private Context context;

    public BoardListAdapter(List<BoardListResult.BoardBean> list, Context context) {
        this.list = list;
        this.context = context;
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int position) {
        return list.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View view, ViewGroup viewGroup) {
        viewHolder holder;
        if (view == null) {
            view = LayoutInflater.from(context).inflate(R.layout.item_board_list_layout, null);
            holder = new viewHolder();
            holder.txtTitle = (TextView) view.findViewById(R.id.item_board_txt_title);
            view.setTag(holder);
        } else {
            holder = (viewHolder) view.getTag();
        }

        holder.txtTitle.setText(list.get(position).title);
        return view;
    }

    class viewHolder {
        TextView txtTitle;
    }
}
