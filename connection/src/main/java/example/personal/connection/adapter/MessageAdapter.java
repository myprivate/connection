package example.personal.connection.adapter;

import android.content.Context;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.hyphenate.chat.EMTextMessageBody;
import com.nostra13.universalimageloader.core.ImageLoader;

import java.util.List;

import example.personal.connection.Constants;
import example.personal.connection.R;
import example.personal.connection.Utils.CommonUtils;
import example.personal.connection.Utils.DateUtils;
import example.personal.connection.module.emchat.IMMessage;

/**
 * @author sunjichang
 * @version 1.0
 * @date 2017/3/1 22:04
 * @upate
 */

public class MessageAdapter extends CommonAdapter<IMMessage>{

    public MessageAdapter(Context context, List<IMMessage> datas, int layoutId) {
        super(context, datas, layoutId);
    }

    @Override
    public void convert(CommonViewHolder holder, IMMessage msg, int position) {
        ImageLoader.getInstance().displayImage(Constants.formatImageUrl(msg.sendPhoto),(ImageView) holder.getView(R.id.img_head),
                CommonUtils.getUserIconOptions());
        holder.setText(R.id.txt_name,msg.sendName);
        EMTextMessageBody body = null;
        if(msg.message != null){
            body = (EMTextMessageBody)msg.message.getBody();
        }
        if(body != null) {
            holder.setText(R.id.txt_content, body.getMessage());
            holder.setText(R.id.txt_time, DateUtils.getChatTime(msg.message.getMsgTime()));
        }else{
            holder.setText(R.id.txt_content,"");
            holder.setText(R.id.txt_time,"");
        }
        TextView txtUnRead = holder.getView(R.id.txt_unread_number);
        txtUnRead.setVisibility(msg.unReadCount > 0 ? View.VISIBLE : View.INVISIBLE);
        txtUnRead.setText(msg.unReadCount+"");
    }
}