package example.personal.connection.adapter;

import android.content.Context;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.nostra13.universalimageloader.core.ImageLoader;

import java.util.List;

import example.personal.connection.Constants;
import example.personal.connection.R;
import example.personal.connection.Utils.CommonUtils;
import example.personal.connection.Utils.DialogUtils;
import example.personal.connection.Utils.DipUtils;
import example.personal.connection.entity.MomentsResultBean;

/**
 * Created by wuqian on 2017/3/3.
 * mail: wuqian@ilingtong.com
 * Description: 朋友圈列表适配器。
 * 显示自己发的朋友圈时隐藏头像
 */
public class MomentsListAdapter extends BaseAdapter {
    private List<MomentsResultBean.MomentsBean> list;
    private Context context;
    private boolean isShowHead;
    private int screenWidth;

    public MomentsListAdapter(List<MomentsResultBean.MomentsBean> list, boolean isShowHead, Context context) {
        this.list = list;
        this.isShowHead = isShowHead;
        this.context = context;

        screenWidth = DipUtils.getScreenWidth(context);
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int position) {
        return list.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View view, ViewGroup viewGroup) {
        viewHolder holder;
        if (view == null) {
            holder = new viewHolder();
            view = LayoutInflater.from(context).inflate(R.layout.item_moment_layout, null);
            holder.itemMomentImgHead = (ImageView) view.findViewById(R.id.item_moment_img_head);
            holder.itemMomentTxtName = (TextView) view.findViewById(R.id.item_moment_txt_name);
            holder.itemMomentTxtTime = (TextView) view.findViewById(R.id.item_moment_txt_time);
            holder.itemMomentTxtContent = (TextView) view.findViewById(R.id.item_moment_txt_content);
            holder.itemMomentImgPhoto = (ImageView) view.findViewById(R.id.item_moment_img_photo);
            holder.itemMomentImgHead.setVisibility(isShowHead ? View.VISIBLE : View.GONE);
            ViewGroup.LayoutParams lp = holder.itemMomentImgPhoto.getLayoutParams();
            lp.width = screenWidth;
            lp.height = ViewGroup.LayoutParams.WRAP_CONTENT;
            holder.itemMomentImgPhoto.setLayoutParams(lp);
            holder.itemMomentImgPhoto.setMaxWidth(screenWidth);
            holder.itemMomentImgPhoto.setMaxHeight((int) (screenWidth * 3));// 这里可以根据需求而定
            view.setTag(holder);
        } else {
            holder = (viewHolder) view.getTag();
        }
        holder.itemMomentTxtName.setText(list.get(position).name + "");
        holder.itemMomentTxtTime.setText(list.get(position).createTime + "");
        holder.itemMomentTxtContent.setText(list.get(position).contentText);
        if (!TextUtils.isEmpty(list.get(position).photo)) {
            ImageLoader.getInstance().displayImage(Constants.formatImageUrl(list.get(position).photo), holder.itemMomentImgHead, CommonUtils.getUserIconOptions());
        }else {
            holder.itemMomentImgHead.setImageResource(R.mipmap.default_head_icon);
        }
        if (TextUtils.isEmpty(list.get(position).contentPicture)) {
            holder.itemMomentImgPhoto.setVisibility(View.GONE);
        } else {
            holder.itemMomentImgPhoto.setVisibility(View.VISIBLE);
            ImageLoader.getInstance().displayImage(Constants.formatImageUrl(list.get(position).contentPicture), holder.itemMomentImgPhoto, CommonUtils.getUserIconOptions());
        }
        return view;
    }

    class viewHolder {
        public ImageView itemMomentImgHead;
        public TextView itemMomentTxtName;
        public TextView itemMomentTxtTime;
        public TextView itemMomentTxtContent;
        public ImageView itemMomentImgPhoto;
    }
}
