package example.personal.connection.adapter;

import android.content.Context;
import android.os.Handler;
import android.os.Message;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.nostra13.universalimageloader.core.ImageLoader;

import java.util.List;

import example.personal.connection.Constants;
import example.personal.connection.R;
import example.personal.connection.Utils.CommonUtils;
import example.personal.connection.activity.SearchActivity;
import example.personal.connection.entity.LoginInfoBean;

/**
 * Created by wuqian on 2017/3/3.
 * mail: wuqian@ilingtong.com
 * Description:搜索好友结果列表适配器
 */
public class SearchResultAdapter extends BaseAdapter {
    private List<LoginInfoBean> list;
    private Handler handler;
    private Context context;

    public SearchResultAdapter(List<LoginInfoBean> list, Handler handler, Context context) {
        this.list = list;
        this.handler = handler;
        this.context = context;
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int position) {
        return list.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View view, ViewGroup viewGroup) {
        viewHolder holder;
        if (view == null) {
            holder = new viewHolder();
            view = LayoutInflater.from(context).inflate(R.layout.item_search_layout, null);
            holder.txt_add = (TextView) view.findViewById(R.id.item_search_txt_add);
            holder.txt_name = (TextView) view.findViewById(R.id.item_search_txt_name);
            holder.img_head = (ImageView) view.findViewById(R.id.item_search_img_head);
            view.setTag(holder);
        } else {
            holder = (viewHolder) view.getTag();
        }
        holder.txt_add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Message msg = new Message();
                msg.what = SearchActivity.WHAT_ADD;
                msg.arg1 = position;
                handler.sendMessage(msg);
            }
        });
        holder.txt_name.setText(list.get(position).name);
        if (!TextUtils.isEmpty(list.get(position).photo)) {
            ImageLoader.getInstance().displayImage(Constants.formatImageUrl(list.get(position).photo), holder.img_head, CommonUtils.getUserIconOptions());
        }else {
            holder.img_head.setImageResource(R.mipmap.default_head_icon);
        }
        return view;
    }

    class viewHolder {
        TextView txt_name;
        ImageView img_head;
        TextView txt_add;
    }
}
