package example.personal.connection.adapter;

/**
 * 通用适配器
 */
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import java.util.List;

public abstract class CommonAdapter<T> extends BaseAdapter {

	protected Context mContext;
	protected LayoutInflater mInflater;
	protected List<T> mDatas;
	private int layoutId;

	public CommonAdapter(Context context, List<T> datas, int layoutId) {
		mContext = context;
		mInflater = LayoutInflater.from(context);
		mDatas = datas;
		this.layoutId = layoutId;
	}

	public void setData(List<T> datas) {
		mDatas = datas;
		notifyDataSetChanged();
	}
	
	public void refreshData(List<T> datas){
		if(null == datas){
			return;
		}
		if(null == mDatas){
			mDatas.clear();
		}
		setData(datas);
	}

	@Override
	public int getCount() {
		return mDatas != null ? mDatas.size() : 0;
	}

	@Override
	public T getItem(int position) {
		return mDatas.get(position);
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		CommonViewHolder holder = CommonViewHolder.get(mContext, convertView, parent, layoutId, position);
		convert(holder, getItem(position),position);
		return holder.getContentView();
	}

	public abstract void convert(CommonViewHolder holder, T t,int position);

}
