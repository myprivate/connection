package example.personal.connection.adapter;

import android.content.Context;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.nostra13.universalimageloader.core.ImageLoader;

import java.util.List;

import example.personal.connection.Constants;
import example.personal.connection.R;
import example.personal.connection.Utils.CommonUtils;
import example.personal.connection.entity.LoginInfoBean;

/**
 * Created by wuqian on 2017/2/28.
 * mail: wuqian@ilingtong.com
 * Description:我的校友列表adapter
 */
public class ContactAdapter extends BaseAdapter {
    private Context context;
    List<LoginInfoBean> list;

    public ContactAdapter(Context context, List<LoginInfoBean> list) {
        this.context = context;
        this.list = list;
    }

    public void setList(List<LoginInfoBean> list){
        if(null != list){
            this.list = list;
            notifyDataSetChanged();
        }
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int position) {
        return list.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View view, ViewGroup viewGroup) {
        viewHolder holder;
        if (view == null) {
            holder = new viewHolder();
            view = LayoutInflater.from(context).inflate(R.layout.item_contact_layout, null);
            holder.img_pic = (ImageView) view.findViewById(R.id.item_contact_img_head);
            holder.txt_title = (TextView) view.findViewById(R.id.item_contact_txt_name);
            view.setTag(holder);
        } else {
            holder = (viewHolder) view.getTag();
        }
        holder.txt_title.setText(list.get(position).name);
        if (!TextUtils.isEmpty(list.get(position).photo)) {
            ImageLoader.getInstance().displayImage(Constants.formatImageUrl(list.get(position).photo), holder.img_pic, CommonUtils.getUserIconOptions());
        }else {
            holder.img_pic.setImageResource(R.mipmap.default_head_icon);
        }
        return view;
    }

    class viewHolder {
        ImageView img_pic;
        TextView txt_title;
    }
}
