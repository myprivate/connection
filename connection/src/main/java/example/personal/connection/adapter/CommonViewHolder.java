package example.personal.connection.adapter;
import android.content.Context;
import android.graphics.Bitmap;
import android.util.SparseArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;


public class CommonViewHolder{
	private SparseArray<View> mViews;
	private int mPosition;
	private View mContentView;
	private ImageLoader mImageLoader = ImageLoader.getInstance();
	public CommonViewHolder(Context context,ViewGroup parent,int layoutId,int position){
		this.mPosition = position;
		this.mViews = new SparseArray<View>();
		mContentView = LayoutInflater.from(context).inflate(layoutId, parent, false);
		mContentView.setTag(this);
	}
	
	public static CommonViewHolder get(Context context,View contentView,ViewGroup parent,int layoutId,int position){
		if(contentView == null){
			return new CommonViewHolder(context, parent, layoutId, position);
		}else{
			CommonViewHolder holder = (CommonViewHolder) contentView.getTag();
			holder.mPosition = position;
			return holder;
		}
		
	}
	
	/**
	 * 通过ViewId获取控件
	 * @param viewId
	 * @return
	 */
	public <T extends View> T getView(int viewId){
		View view = mViews.get(viewId);
		if(view == null){
			view = mContentView.findViewById(viewId);
			mViews.put(viewId, view);
		}
		return (T)view;
		
	}
	
	public View getContentView(){
		return mContentView;
	}
	
	public CommonViewHolder setText(int viewId,String text){
		TextView tv = getView(viewId);
		tv.setText(text);
		return this;
	}
	
	public CommonViewHolder setImageResource(int viewId,int resId){
		ImageView im = getView(viewId);
		im.setImageResource(resId);
		return this;
	}
	
	public CommonViewHolder setImageBitmap(int viewId,Bitmap bitmap){
		ImageView im = getView(viewId);
		im.setImageBitmap(bitmap);
		return this;
	}
	
	public CommonViewHolder setImageUrl(int viewId,String url){
		ImageView im = getView(viewId);
		//im.setImageResource(R.drawable.default_advise);
		mImageLoader.displayImage(url, im);
		return this;
	}
	public CommonViewHolder setImageUrl(int viewId,String url,DisplayImageOptions mOptions){
		ImageView im = getView(viewId);
		mImageLoader.displayImage(url, im, mOptions);
		return this;
	}
	
	public int getmPosition() {
		return mPosition;
	}
	
}
