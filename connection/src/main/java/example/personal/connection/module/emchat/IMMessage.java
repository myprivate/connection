package example.personal.connection.module.emchat;

import android.os.Parcel;
import android.os.Parcelable;

import com.hyphenate.chat.EMMessage;
import com.hyphenate.chat.EMTextMessageBody;
import com.hyphenate.exceptions.HyphenateException;

/**
 * @author sunjichang
 * @version 1.0
 * @date 2017/3/3 12:53
 * @upate
 */

public class IMMessage implements Parcelable{
    /**
     * 发送者id
     */
    public String sendId;
    /**
     * 发送者voip账号
     */
    public String sendUserName;
    /**
     * 发送者姓名
     */
    public String sendName;
    /**
     * 发送者头像
     */
    public String sendPhoto;
    public String receiveId;
    public String receiveUserName;
    public String receiveName;
    public String receivePhoto;
    public EMMessage message;
    public int unReadCount;

    public IMMessage(){

    }

    public IMMessage(Parcel p){
        sendId = p.readString();
        sendName = p.readString();
        sendPhoto = p.readString();
        sendUserName = p.readString();
        receiveId = p.readString();
        receiveName = p.readString();
        receivePhoto = p.readString();
        receiveUserName = p.readString();
        unReadCount = p.readInt();
        message = p.readParcelable(IMMessage.class.getClassLoader());
    }

    public IMMessage(EMMessage message,boolean reverse){
        try {
            this.sendName = message.getStringAttribute(reverse ? "receiveName" : "sendName");
            this.sendPhoto = message.getStringAttribute(reverse ? "receivePhoto" : "sendPhoto");
            this.sendId = message.getStringAttribute(reverse ? "receiveId" : "sendId");
            this.sendUserName = reverse ? message.getTo() : message.getFrom();
            this.receiveUserName = reverse ? message.getFrom() : message.getTo();
            this.receivePhoto = message.getStringAttribute(reverse ? "sendPhoto" :"receivePhoto");
            this.receiveId = message.getStringAttribute(reverse ? "sendId" : "receiveId");
            this.receiveName = message.getStringAttribute(reverse ? "sendName" : "receiveName");
        } catch (HyphenateException e) {
            e.printStackTrace();
        }
        this.message =message;
    }

    @Override
    public String toString() {
        return "sendId="+sendId+",sendUserName="+sendUserName+",sendName="+sendName
                +",sendPhoto="+sendPhoto+",receiveId="+receiveId+",receiveUserName="+receiveUserName
                +",receiveName="+receiveName+",receivePhoto="+receivePhoto+",unReadCount="+unReadCount+
                ",mess="+(null == message ? "" : ((EMTextMessageBody)message.getBody()).getMessage());
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(sendId);
        dest.writeString(sendName);
        dest.writeString(sendPhoto);
        dest.writeString(sendUserName);
        dest.writeString(receiveId);
        dest.writeString(receiveName);
        dest.writeString(receivePhoto);
        dest.writeString(receiveUserName);
        dest.writeInt(unReadCount);
        dest.writeParcelable(message,flags);
    }

    public static final Parcelable.Creator<IMMessage> CREATOR = new Creator<IMMessage>() {

        @Override
        public IMMessage[] newArray(int size) {
            // TODO Auto-generated method stub
            return new IMMessage[size];
        }

        @Override
        public IMMessage createFromParcel(Parcel source) {
            // TODO Auto-generated method stub
            return new IMMessage(source);
        }
    };
}
