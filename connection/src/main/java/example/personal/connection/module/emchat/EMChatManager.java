package example.personal.connection.module.emchat;

import android.app.Activity;
import android.app.ActivityManager;
import android.app.Application;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.graphics.BitmapFactory;
import android.os.Handler;
import android.os.Message;
import android.support.v7.app.NotificationCompat;
import android.util.Log;
import android.widget.RemoteViews;

import com.hyphenate.EMCallBack;
import com.hyphenate.EMConnectionListener;
import com.hyphenate.EMError;
import com.hyphenate.EMMessageListener;
import com.hyphenate.chat.EMClient;
import com.hyphenate.chat.EMCmdMessageBody;
import com.hyphenate.chat.EMMessage;
import com.hyphenate.chat.EMOptions;
import com.hyphenate.chat.EMTextMessageBody;
import com.hyphenate.exceptions.HyphenateException;
import com.hyphenate.util.NetUtils;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.assist.ImageSize;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import example.personal.connection.Constants;
import example.personal.connection.R;
import example.personal.connection.Utils.CommonUtils;
import example.personal.connection.activity.IMChatActivity;
import example.personal.connection.receiver.EMChatMessageNotificationClickReceiver;

import static android.content.Context.ACTIVITY_SERVICE;
import static android.content.Context.NOTIFICATION_SERVICE;

/**
 * @author sunjichang
 * @version 1.0
 * @date 2017/3/1 01:32
 * @upate
 */

public class EMChatManager {
    public static final int CHATTYPE_GROUP = 1;
    public static final int CHATTYPE_FRIEND = 0;
    private static EMChatManager mInstance = null;
    private static List<EMMessageListener> mListeners = null;
    private Context mContext;
    //通知栏上通知条数
    private int mNotificationCount = 0;
    private final int mNotificationId = 1000;
    private HashMap<String,ArrayList<EMMessage>> mMessageMap = null;
    private FlushConversationListener mFlushListener = null;
    private Handler mHandler = new Handler(new Handler.Callback() {
        @Override
        public boolean handleMessage(Message msg) {
            List<EMMessage> list = (List<EMMessage>) msg.obj;
            showNotification(list);
            return false;
        }
    });

    private static class InstanceHolder{
        static final EMChatManager INSTANCE = new EMChatManager();
    }

    private EMChatManager(){
        mListeners = new ArrayList<>();
    }

    /**
     * 获取单利模式
     * @return
     */
    public static EMChatManager getInstance(){
        return InstanceHolder.INSTANCE;
    }

    /**
     * 初始化环信sdk
     * @param context
     */
    public void init(final Application context){
        mContext = context;
        mMessageMap = new HashMap<String,ArrayList<EMMessage>>();
        EMOptions options = new EMOptions();
        // 默认添加好友时，是不需要验证的，改成需要验证
        options.setAcceptInvitationAlways(false);
        //设置是否允许自动登录
        options.setAutoLogin(false);
        //初始化
        EMClient.getInstance().init(context, options);
        //在做打包混淆时，关闭debug模式，避免消耗不必要的资源
        EMClient.getInstance().setDebugMode(true);
        //添加消息监听器
        EMClient.getInstance().chatManager().addMessageListener(messageListener);
        //添加监听器
        EMClient.getInstance().addConnectionListener(new EMConnectionListener() {
            @Override
            public void onConnected() {

            }

            @Override
            public void onDisconnected(int errorCode) {
                if(errorCode == EMError.USER_REMOVED){
                    // 显示帐号已经被移除
                }else if (errorCode == EMError.USER_LOGIN_ANOTHER_DEVICE) {
                    // 显示帐号在其他设备登录
                } else {
                    if (NetUtils.hasNetwork(context.getApplicationContext())) {
                        //连接不到聊天服务器
                    }else{
                        //当前网络不可用，请检查网络设置
                    }
                }
            }
        });
    }

    /**
     * 添加监听
     * @param listener
     */
    public void addMessageListener(EMMessageListener listener){
        if(null != listener){
            mListeners.add(listener);
        }
    }

    /**
     * 删除最后添加的一个监听
     */
    public void popMessageListener(){
        if(null != mListeners && mListeners.size() > 0){
            mListeners.remove(mListeners.size() - 1);
        }
    }

    public void removeMessageListener(){
        EMClient.getInstance().chatManager().removeMessageListener(messageListener);
    }

    /**
     * 登录到环信服务器
     * @param userName 用户名
     * @param password 密码
     */
    public void loginEM(String userName,String password){
        Log.e("TAG","登录聊天服务器");
        EMClient.getInstance().login(userName,password,new EMCallBack() {//回调
            @Override
            public void onSuccess() {
                //EMClient.getInstance().groupManager().loadAllGroups();
                EMClient.getInstance().chatManager().loadAllConversations();
                Log.e("main", "登录聊天服务器成功！");
            }

            @Override
            public void onProgress(int progress, String status) {

            }

            @Override
            public void onError(int code, String message) {
                Log.e("main", "登录聊天服务器失败！message="+message+",code="+code);
            }
        });
    }

    /**
     * 发送透传消息
     * @param receive 接受者
     * @param sendName 发送人姓名
     * @param photo 发送人头像
     * @param sendId 发送人id
     */
    public void sendPenetrateMsg(String receive,String sendName,String photo,String sendId){
        EMMessage cmdMsg = EMMessage.createSendMessage(EMMessage.Type.CMD);
        //支持单聊和群聊，默认单聊，如果是群聊添加下面这行
        //cmdMsg.setChatType(EMMessage.ChatType.GroupChat);
        String action="action1";//action可以自定义
        EMCmdMessageBody cmdBody = new EMCmdMessageBody(action);
        cmdMsg.addBody(cmdBody);
        cmdMsg.setFrom(receive);
        EMClient.getInstance().chatManager().sendMessage(cmdMsg);
    }

    /**
     * 同步退出登录
     */
    public void syncLoginOutEM(){
        EMClient.getInstance().logout(true);
    }

    /**
     * 异步退出登录
     */
    public void aSyncLoginOutEM(){
        EMClient.getInstance().logout(true, new EMCallBack() {
            @Override
            public void onSuccess() {

            }

            @Override
            public void onError(int code, String error) {

            }

            @Override
            public void onProgress(int progress, String status) {

            }
        });
    }

    /**
     * 发送消息
     * @param content 内容
     * @param receive 接受者im账号
     * @param sendName 发送人姓名
     * @param sendPhoto 发送人头像
     * @param sendId 发送人id
     * @param chatType
     */
    public void sendMsg(String content,String receive,String sendName,
                        String sendPhoto,String sendId,
                        String receiveName,String receivePhoto,String receiveId
                        ,int chatType,final IMCallback callBack){
        //创建一条文本消息，content为消息文字内容，toChatUsername为对方用户或者群聊的id，后文皆是如此
        final EMMessage message = EMMessage.createTxtSendMessage(content, receive);
        message.setAttribute("sendName",sendName);
        message.setAttribute("sendPhoto",sendPhoto);
        message.setAttribute("sendId",sendId);
        message.setAttribute("receiveName",receiveName);
        message.setAttribute("receivePhoto",receivePhoto);
        message.setAttribute("receiveId",receiveId);
        //如果是群聊，设置chattype，默认是单聊
        if (chatType == CHATTYPE_GROUP) {
            message.setChatType(EMMessage.ChatType.GroupChat );
        }
        Log.e("TAG","sendMsg content="+content+",receive="+receive+",sendName="+sendName);
        message.setMessageStatusCallback(new EMCallBack() {
            @Override
            public void onSuccess() {
                if(null != callBack){
                    callBack.onSuccess(message);
                }
                Log.e("TAG","onSuccess");
            }

            @Override
            public void onError(int code, String error) {
                if(null != callBack){
                    callBack.onError(code,error);
                }
                Log.e("TAG","onError code="+code+",error="+error);
            }

            @Override
            public void onProgress(int progress, String status) {
                if(null != callBack){
                    callBack.onProgress(progress,status);
                }
                Log.e("TAG","onProgress progress="+progress+",status="+status);
            }
        });
        //发送消息
        EMClient.getInstance().chatManager().sendMessage(message);
    }


    /**
     * 消息接受监听器
     */
    private EMMessageListener messageListener = new EMMessageListener() {
        /**
         * 收到消息
         * @param messages
         */
        @Override
        public void onMessageReceived(List<EMMessage> messages) {
            //EMClient.getInstance().chatManager().importMessages(messages);
            Log.e("TAG","EMChatM onMessageReceived=");
            if (null != mListeners){
                for (int i = 0;i < mListeners.size(); i++) {
                    mListeners.get(i).onMessageReceived(messages);
                }
            }
            Message msg = mHandler.obtainMessage();
            msg.obj = messages;
            mHandler.sendMessage(msg);
            //showNotification(messages);
        }

        /**
         * 收到透传消息
         * @param messages
         */
        @Override
        public void onCmdMessageReceived(List<EMMessage> messages) {
            if (null != mListeners){
                for (int i = 0;i < mListeners.size(); i++) {
                    mListeners.get(i).onCmdMessageReceived(messages);
                }
            }
        }

        /**
         * 收到已读回执
         * @param messages
         */
        @Override
        public void onMessageRead(List<EMMessage> messages) {
            if (null != mListeners){
                for (int i = 0;i < mListeners.size(); i++) {
                    mListeners.get(i).onMessageRead(messages);
                }
            }
        }

        /**
         * 收到已送达回执
         * @param messages
         */
        @Override
        public void onMessageDelivered(List<EMMessage> messages) {
            if (null != mListeners){
                for (int i = 0;i < mListeners.size(); i++) {
                    mListeners.get(i).onMessageDelivered(messages);
                }
            }
        }

        /**
         * 消息状态变动
         * @param message
         * @param change
         */
        @Override
        public void onMessageChanged(EMMessage message, Object change) {
            if (null != mListeners){
                for (int i = 0;i < mListeners.size(); i++) {
                    mListeners.get(i).onMessageChanged(message,change);
                }
            }
        }
    };

    private void showNotification(List<EMMessage> messages){
        Activity activity = getCurrentActivity();
        Log.e("TAG","showNotification activity="+activity);
        //API level 21. 以后被遗弃了
        ActivityManager am = (ActivityManager) mContext.getSystemService(ACTIVITY_SERVICE);
        ComponentName cn = am.getRunningTasks(1).get(0).topActivity;
        Log.e("TAG","cn="+cn);
        if(null == activity && !isRunningInBackground()){
            return;
        }
        if(null != activity && activity.getClass().equals(IMChatActivity.class)){
            return;
        }
        mNotificationCount = getSendPeopleCount(messages);
        //第一步：获取状态通知栏管理：
        NotificationManager nm = (NotificationManager) mContext.getSystemService(NOTIFICATION_SERVICE);
        nm.cancel(mNotificationId);
        String title = null;
        NotificationCompat.Builder builder = new NotificationCompat.Builder(mContext);
        Intent bIntent = new Intent(mContext,EMChatMessageNotificationClickReceiver.class);
        bIntent.putExtra("content",mMessageMap);
        PendingIntent intent = PendingIntent.getBroadcast(mContext,1000,
                bIntent,PendingIntent.FLAG_UPDATE_CURRENT);
        if(mNotificationCount == 1){
            sort(messages);
            EMMessage msg = messages.get(0);
            RemoteViews views = null;
            String content = null;
            String sendPhoto = null;
            try {
                title = msg.getStringAttribute("sendName");
                content = ((EMTextMessageBody)msg.getBody()).getMessage();
                sendPhoto = msg.getStringAttribute("sendPhoto");
//                views = new RemoteViews(mContext.getPackageName(),R.layout.layout_message_notification);
//                if(CommonUtils.isEmpty(sendPhoto)){
//                    views.setImageViewResource(R.id.img_photo,R.mipmap.default_head_icon);
//                }else{
//                    views.setImageViewUri(R.id.img_photo, Uri.parse(Constants.formatImageUrl(sendPhoto)));
//                }
//                views.setTextViewText(R.id.txt_name,title);
//                views.setTextViewText(R.id.txt_content,"["+getMessageCount()+"条]"+title+":"+content);
//                views.setOnClickPendingIntent(R.id.layout_root,intent);
            } catch (HyphenateException e) {
                e.printStackTrace();
            }
//            builder.setContent(views);
            Log.e("TAG","sendPhoto="+sendPhoto+",sendName="+sendPhoto);
            builder.setContentTitle(title)
                    .setContentText("["+getMessageCount()+"条]"+title+":"+content);
            if(CommonUtils.isEmpty(sendPhoto)){
                builder.setLargeIcon(BitmapFactory.decodeResource(mContext.getResources(),R.mipmap.ic_launcher));
            }else{
                builder.setLargeIcon(ImageLoader.getInstance().loadImageSync(Constants.formatImageUrl(sendPhoto),
                        new ImageSize(50,50)));
            }
            builder.setContentIntent(intent);
            builder.setSmallIcon(R.mipmap.ic_launcher)
                    .setWhen(System.currentTimeMillis());
        }else{
            title = mContext.getString(R.string.app_name);
            builder.setContentTitle(title)
                    .setLargeIcon(BitmapFactory.decodeResource(mContext.getResources(),R.mipmap.ic_launcher))
                    .setContentText("有"+mNotificationCount+"个联系人发来"+getMessageCount()+"条信息");
            builder.setContentIntent(intent);
            builder.setSmallIcon(R.mipmap.ic_launcher)
                    .setWhen(System.currentTimeMillis());
        }
        Notification notification = builder.build();
        //点击后自动销毁
        notification.flags |= Notification.FLAG_AUTO_CANCEL;
        nm.notify(mNotificationId,notification);
    }

    /**
     * 获取发送消息人的数量
     * @param messages
     * @return
     */
    private int getSendPeopleCount(List<EMMessage> messages){
        if(messages == null){
            return 0;
        }
        int count = messages.size();
        EMMessage msg = null;
        ArrayList<EMMessage> listMsg = null;
        String from = null;
        for (int i = 0; i < count; i++){
            msg = messages.get(i);
            from = msg.getFrom();
            listMsg = mMessageMap.get(from);
            if(null == listMsg){
                listMsg = new ArrayList<>();
            }
            listMsg.add(msg);
            mMessageMap.put(msg.getFrom(),listMsg);
        }
        return mMessageMap.size();
    }

    /**
     * 获取消息数量
     * @return
     */
    private int getMessageCount(){
        if(mMessageMap == null){
            return 0;
        }
        Set<String> set = mMessageMap.keySet();
        List<EMMessage> list = null;
        int count = 0;
        for (Iterator<String> i= set.iterator();i.hasNext();){
            list = mMessageMap.get(i.next());
            count += list.size();
        }
        return count;
    }

    /**
     * 按照时间的先后顺序排序
     * @param messages
     */
    private void sort(List<EMMessage> messages){
        if(null == messages){
            return;
        }
        Collections.sort(messages, new Comparator<EMMessage>() {
            @Override
            public int compare(EMMessage o1, EMMessage o2) {
                if(o1.getMsgTime() > o2.getMsgTime()){
                    return 1;
                }else if(o1.getMsgTime() < o2.getMsgTime()){
                    return -1;
                }
                return 0;
            }
        });
    }

    /**
     * 清除通知数据
     */
    public void clearNotificationData(){
        mNotificationCount = 0;
        mMessageMap.clear();
    }

    /**
     * 清除通知栏通知
     */
    public void clearNotification(){
        NotificationManager nm = (NotificationManager) mContext.getSystemService(NOTIFICATION_SERVICE);
        nm.cancel(mNotificationId);
    }

    /**
     * 获取当前应用activity对象
     * @return
     */
    public static Activity getCurrentActivity () {
        try {
            Class activityThreadClass = Class.forName("android.app.ActivityThread");
            Object activityThread = activityThreadClass.getMethod("currentActivityThread").invoke(
                    null);
            Field activitiesField = activityThreadClass.getDeclaredField("mActivities");
            activitiesField.setAccessible(true);
            Map activities = (Map) activitiesField.get(activityThread);
            for (Object activityRecord : activities.values()) {
                Class activityRecordClass = activityRecord.getClass();
                Field pausedField = activityRecordClass.getDeclaredField("paused");
                pausedField.setAccessible(true);
                if (!pausedField.getBoolean(activityRecord)) {
                    Field activityField = activityRecordClass.getDeclaredField("activity");
                    activityField.setAccessible(true);
                    Activity activity = (Activity) activityField.get(activityRecord);
                    return activity;
                }
            }
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        } catch (NoSuchFieldException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }
        return null;
    }

    private void printActivePackages(){
        ActivityManager am = (ActivityManager)mContext.getSystemService(Context.ACTIVITY_SERVICE);
        List<ActivityManager.RunningAppProcessInfo> list = am.getRunningAppProcesses();
        ActivityManager.RunningAppProcessInfo info = null;
        for (int i = 0; i < list.size(); i++){
            info = list.get(i);
            Log.e("TAG","INFO.processName="+info.processName);
            String[] strs = info.pkgList;
            for (int j = 0; j < strs.length;j++){
                Log.e("TAG","pkgList["+j+"]="+strs[j]);
            }
        }
    }

    /**
     * 当前app是否处于后台运行
     * @return
     */
    private boolean isRunningInBackground(){
        ActivityManager am = (ActivityManager)mContext.getSystemService(Context.ACTIVITY_SERVICE);
        List<ActivityManager.RunningAppProcessInfo> list = am.getRunningAppProcesses();
        ActivityManager.RunningAppProcessInfo info = null;
        String currentPkName = mContext.getPackageName();
        for (int i = 0; i < list.size(); i++){
            info = list.get(i);
            String processName = info.processName;
            if(processName.contains(currentPkName)){
                return true;
            }
        }
        return false;
    }

    /**
     * 添加会话列表刷新监听
     * @param listener
     */
    public void addConversationFlushListener(FlushConversationListener listener){
        mFlushListener = listener;
    }

    /**
     * 刷新会话列表
     */
    public void flushConversation(){
        if(null != mFlushListener){
            mFlushListener.flush();
        }
    }

    public interface FlushConversationListener{
        public void flush();
    }

    public interface IMCallback{
        public void onSuccess(EMMessage message);
        public void onProgress(int progress, String status);
        public void onError(int code, String message);
    }
}
