package example.personal.connection.receiver;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import com.hyphenate.chat.EMMessage;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Set;

import example.personal.connection.activity.IMChatActivity;
import example.personal.connection.activity.MainFragmentActivity;
import example.personal.connection.module.emchat.EMChatManager;
import example.personal.connection.module.emchat.IMMessage;

import static android.content.Intent.FLAG_ACTIVITY_NEW_TASK;

/**
 * 环信通知栏信息点击监听广播
 * @author sunjichang
 * @version 1.0
 * @date 2017/3/5 11:14
 * @upate
 */

public class EMChatMessageNotificationClickReceiver extends BroadcastReceiver{


    @Override
    public void onReceive(Context context, Intent intent) {
        EMChatManager.getInstance().clearNotificationData();
        HashMap<String,ArrayList<EMMessage>> msgMap = (HashMap<String,ArrayList<EMMessage>>)intent.getSerializableExtra("content");
        if(msgMap == null){
            return;
        }
        Intent startIntent = null;
        Set<String> set = msgMap.keySet();
        //如果是单个人的消息，就跳转到聊天界面，否则跳转到主页
        if(msgMap.size() == 1){
            String key = set.iterator().next();
            List<EMMessage> list = msgMap.get(key);
            if(null != list && list.size() > 0){
                EMMessage msg = list.get(0);
                startIntent = new Intent(context, IMChatActivity.class);
                IMMessage imsg = new IMMessage(msg,true);
                startIntent.putExtra("content",imsg);
            }
        }else{
            startIntent = new Intent(context, MainFragmentActivity.class);
        }
        startIntent.addFlags(FLAG_ACTIVITY_NEW_TASK);
        context.startActivity(startIntent);
    }
}
