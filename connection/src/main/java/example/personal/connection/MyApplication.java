package example.personal.connection;

import android.app.Application;
import android.content.Context;

import com.android.volley.RequestQueue;
import com.android.volley.toolbox.Volley;
import com.nostra13.universalimageloader.cache.disc.naming.Md5FileNameGenerator;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.assist.QueueProcessingType;

import example.personal.connection.entity.LoginInfoBean;
import example.personal.connection.module.emchat.EMChatManager;

/**
 * Created by wuqian on 2017/2/27.
 * mail: wuqian@ilingtong.com
 * Description:
 */
public class MyApplication extends Application {
    private static RequestQueue requestQueue;
    private static MyApplication instance;
    private LoginInfoBean userInfo;

    public LoginInfoBean getUserInfo() {
        return userInfo;
    }

    /**
     * 获取用户id
     * @return
     */
    public String getUserId(){
        return userInfo == null ? null : userInfo.id;
    }

    public void setUserInfo(LoginInfoBean userInfo) {
        this.userInfo = userInfo;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        initEMCSDK();
        requestQueue = Volley.newRequestQueue(this);
        instance = this;
        initImageLoader(getApplicationContext());
    }

    /**
     * 初始化环信sdk
     */
    private void initEMCSDK(){
        EMChatManager.getInstance().init(this);
    }

    public static RequestQueue getRequestQueue() {
        return requestQueue;
    }

    public static MyApplication getInstance() {
        return instance;
    }

    /**
     * 初始化ImageLoader
     *
     * @param context
     */
    public static void initImageLoader(Context context) {
        ImageLoaderConfiguration config = new ImageLoaderConfiguration.Builder(context)
                .threadPriority(Thread.NORM_PRIORITY - 2)
                .denyCacheImageMultipleSizesInMemory()
                .diskCacheFileNameGenerator(new Md5FileNameGenerator())
                .diskCacheSize(50 * 1024 * 1024)
                // 50 Mb
                .tasksProcessingOrder(QueueProcessingType.LIFO)
                .writeDebugLogs() // Remove for release app
                .build();
        ImageLoader.getInstance().init(config);
    }
}
